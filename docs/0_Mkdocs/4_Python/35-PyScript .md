---
template: pyscript.html
---

# PyScript

## 1. Example


!!! note ""
    === "Rendu"
        <div>
            <py-script>
                print("Hello World")
            </py-script>
            <br/>
            <p><strong>Today is <label id='date'></label></strong></p>
            <py-script>
            import time
            pyscript.write('date', time.strftime('%d/%m/%Y %H:%M:%S'))
            </py-script>
            <br/>
            <py-script>  
                print("Let's evaluate π :")
                def eval_pi(n):
                    pi = 2
                    for i in range(1,n):
                        pi *= 4 * i ** 2 / (4 * i ** 2 - 1)
                    return pi
                pi = eval_pi(100000)
                s = "&nbsp;" * 10 + f"π is approximately {pi:.5f}"
                print(s)
            </py-script>
            <br/>
            <div id="hey"></div>
            <py-script>
            pyscript.write('hey', f'Hey Mate !')
            </py-script>
        </div>


    === "Code"
        Pour que le code `PyScript`,  soit actif, il faut placer en entête du document `.md` le code yaml suivant :

        ```yaml
        ---
        template: pyscript.html
        ---
        ```
        Dans le reste du document le code PyScript est utilisé de la façon suivante :

        ```html

        # PyScript

        ## Examples

        <div>
        <py-script>
            print("Hello World")
        </py-script>
        <br/>
        <p><strong>Today is <label id='date'></label></strong></p>
        <py-script>
        import time
        pyscript.write('date', time.strftime('%d/%m/%Y %H:%M:%S'))
        </py-script>
        <br/>

        <py-script>  
            print("Let's evaluate π :")
            def eval_pi(n):
                pi = 2
                for i in range(1,n):
                    pi *= 4 * i ** 2 / (4 * i ** 2 - 1)
                return pi
            pi = eval_pi(100000)
            s = "&nbsp;" * 10 + f"π is approximately {pi:.5f}"
            print(s)
        </py-script>
        <br/>

        <div id="hey"></div>
        <py-script>
        pyscript.write('hey', f'Hey Mate !')
        </py-script>
        </div>
        ```



## 2. Intallation
Si vous utilisez l'image docker associée à ce site et le dépot de basse de ce site tout est installé et configurée ! La note ci-dessous vous est donc inutile.

??? note "Installation"
    Pour pouvoir utiliser PyScript, deux étapes sont essentielles : 
    - modifier le fichier de configuration `mkdocs.yml`
    - créer un template html

    ## Modification du fichier de configuration `mkdocs.yml`

    Dans le fichier de configurations `mkdocs.yml` , il est nécéssaire d'ajouter dans les extra_css, le lien `https://pyscript.net/alpha/pyscript.css` :

    ```yaml
    theme:
    name: material
    custom_dir: my_theme_customizations

    extra_css:
        - https://pyscript.net/alpha/pyscript.css
    ```

    ## Create a `pyscript.html` file

    In the `overrides/` directory, besides `main.html`, create a `pyscript.html` file :

    ```html
    {% raw %}
    {% extends "main.html" %}

    {% block scripts %}
        {{ super() }}
        <script defer src="https://pyscript.net/alpha/pyscript.js"></script>
    {% endblock %}
    {% endraw %}
    ```


## 3. Remarques
Dans la plupart des cas, il est préférables pour des raisons de performance d'utiliser prioritairement les macros.

Cependant, PyScript peut être pratique pour intégrer des traveaux ponctuels et/ou temporaires.

## 4. Ressources
??? note "Ressources"
    - Merci à Rodrigo SCHWENCKE dont cette pages est en très grande partie issu : [https://eskool.gitlab.io/mkhack3rs/pyscript/](https://eskool.gitlab.io/mkhack3rs/pyscript/)
    - [pyscript.net](https://pyscript.net/)



à traiter 
[https://ericecmorlaix.github.io/tests_PyScript/](https://ericecmorlaix.github.io/tests_PyScript/)
sur le forum [https://mooc-forums.inria.fr/moocnsi/t/pyscript-du-python-dans-du-html/6273/20](https://mooc-forums.inria.fr/moocnsi/t/pyscript-du-python-dans-du-html/6273/20)
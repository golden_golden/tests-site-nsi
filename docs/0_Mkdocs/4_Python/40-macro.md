# Macro

## 1. Les Macro

On peut créer sa propre macro Python



## 2. Création de la macro `main.py`
On ajoute une nouvelle macro, dans le fichier `main.py`. Par exemple :

```py
    @env.macro
    def affiche_addition(x, y):
        texte = []
        texte.append("Voici une addition")
        texte.append("")
        texte.append(f"{x} + {y} = {x + y}")
        texte.append("")
        texte.append("Avec LaTeX")
        texte.append("")
        texte.append(f"$${x} + {y} = {x + y}$$")
        return "\n".join(texte)
```
## 3. Utilisation et rendu de la macro

----
**Rendu**

{{ affiche_addition(30, 12) }}

---

!!! note ""
    === "Code"
        Une fois la macro créer dans le fichier `main.py`.

        Pour l'utiliser et obtenir le résultat ci-dessus, il suffit de saisir dans la page `.md` le code suivant :

        ```markdown

        {{ "{{ affiche_addition(30, 12) }}" }}
        ```





## 4. Remarques

!!! important "Remarque"
    À chaque nouvel ajout de macro, il faut alors stopper `mkdocs serve` puis le relancer.


Il est délicat (mais possible) de l'avoir dans une admonition. Pour commencer, utiliser des macros qui renvoient plusieurs lignes de texte **hors** admonition et autre cadre avec indentation.


## 5. Installation
Si vous utilisez l'image docker associée à ce site et le dépot de basse de ce site tout est installé et configurée ! La note ci-dessous vous est donc inutile.

??? note "Installation des macro"
    À faire

## 6. Ressources
??? note "Ressources"
    - Merci à Franck CHAMBON et à sont dépot : [https://gitlab.com/ens-fr/exp2/-/tree/main/](https://gitlab.com/ens-fr/exp2/-/tree/main/)
    - Pour de plus amples informations, se référer à la documentation : [https://mkdocs-macros-plugin.readthedocs.io/en/latest/](https://mkdocs-macros-plugin.readthedocs.io/en/latest/)

title: Ide et console

---
# Pyodide


## 1. Exemple d'exercices


On peut avoir un IDE avec un fichier Python préchargé.

Avec un fichier `exemple.py` qui se trouve dans le même dossier que votre `page.md` à créer.

On ajoute une ligne avec `{{ "{{ IDE('exemple') }}" }}`

{{ IDE('python/exemple') }}


Pour de plus amples informations, le travail de Vincent Bouillot est présenté ici : <https://bouillotvincent.gitlab.io/pyodide-mkdocs/>

<br><br>

## 1. Exemple d'exercices
*Auteur: Romain JANVIER*

Chez le dentiste, la bouche grande ouverte, lorsqu'on essaie de parler, il
ne reste que les voyelles. Même les ponctuations sont supprimées. 
Vous devez écrire une fonction `dentiste(texte)` qui renvoie un texte 
ne contenant que les voyelles de `texte`, dans l'ordre. 

Les voyelles sont données par :
```python
voyelles = ['a', 'e', 'i', 'o', 'u', 'y']
```
On ne considèrera que des textes écrits en minuscules, sans accents.

!!! example "Exemples"

    ```pycon
    >>> dentiste("j'ai mal")
    'aia'
    >>> dentiste("il fait chaud")
    'iaiau'
    >>> dentiste("")
    ''
    ```

{{ IDE('python/exo') }}

## 3. Remarques

## 4. Installation

Si vous utilisez l'image docker associer à ce site et le dépot de basse de ce site tout est installé et configurée ! La note ci-dessous vous est donc inutile.



---- À faire

??? help "Installation"

    Dans un premier temps, pour que les QCM puissent fonctionner il est nécéssaire :

    - installer le plugin  macro de mkdocs : `mkdocs-macros-plugin`
    - installer : `MathJax 3.0`

    <br>

    Dans un second temps, il est également nécéssaire d'apporter les modifications suivantes :

    === "mkdocs.yml"

        Ajouter le lien vers le fichier de css du QCM :

        ```yml
            extra_css:
                ....
                - xtra/stylesheets/qcm.css
                ....
        ```
    
    === "my_theme_customizations/main.html"

        Dans la partie de chargement des libraires (`md{%raw%}{% block libs %}{%endraw%}`), ajouter :
        
        ```html
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.5.0/styles/atom-one-light.min.css">
            <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.5.0/highlight.min.js"></script>  
        ```

    === "my_theme_customizations/js/ide.js"

        À la fin du fichier, ajouter :

        ```javascript
            document.addEventListener('DOMContentLoaded', () => {
                document.querySelectorAll('pre code.qcm').forEach((el) => {
                hljs.highlightElement(el);
                });
            });
            
            document.querySelectorAll("[id^=qcm_]").forEach((el) => {
                let qcmAns = el.childNodes;
                if (el.dataset.shuffle == 1) {
                for (let i = qcmAns.length - 1; i >= 0; i--) el.appendChild(qcmAns[Math.floor(Math.random() * i)])
                }
                
                for (let element of el.children) {
                element.addEventListener('click', () => {
                    element.firstChild.disabled = true
                    element.firstChild.checked = true
                })
                }
            });
        ```

    === "docs/xtra/stylesheets/qcm.css"

        Télécharger le fichier appelé [`qcm.css`](https://gitlab.com/bouillotvincent/pyodide-mkdocs/-/raw/main/docs/xtra/stylesheets/qcm.css "Des supers styles pour QCM").

    === "main.py"

        À la suite de votre configuration existante et au sein de la fonction `define_env`, ajouter le contenu du fichier [`main_QCM.py`](https://gitlab.com/bouillotvincent/pyodide-mkdocs/-/raw/main/docs/scripts/main_qcm.py "Faire des QCM").
    
    === "MathJax"

        Pour rappel, votre fichier de configuration `docs/xtra/javascripts/mathjax-config.js` doit ressembler à cela :

        ```js
        window.MathJax = {
        startup: {
            ready: () => {
                console.log('MathJax is loaded, but not yet initialized');
                MathJax.startup.defaultReady();
                console.log('MathJax is initialized, and the initial typeset is queued');
            }
        },
        tex: {
            inlineMath: [["\\(", "\\)"]],
            displayMath: [["\\[", "\\]"]],
            processEscapes: true,
            processEnvironments: true
        },
        options: {
            ignoreHtmlClass: ".*|",
            processHtmlClass: "arithmatex"
        }
        };

        document$.subscribe(() => {
        MathJax.typesetPromise()
        })
        ```




## 5. Ressources
??? note "Ressources"
    - La totalité de ce est présenté ci-dessus est issus du travail de *Vincent Bouillot* : [Site de Vincent Bouillot](https://bouillotvincent.gitlab.io/pyodide-mkdocs/qcm/) dont le dépot est [https://gitlab.com/bouillotvincent/pyodide-mkdocs/-/tree/main](https://gitlab.com/bouillotvincent/pyodide-mkdocs/-/tree/main)
    
    - Merci au forum de NSI sans qui je n'aurrai pas découvert cet outil : [Forum NSI](https://mooc-forums.inria.fr/moocnsi/) [le fil sur pyodide](https://mooc-forums.inria.fr/moocnsi/t/resolu-installation-de-mkdocs-pyodide/5716/102)





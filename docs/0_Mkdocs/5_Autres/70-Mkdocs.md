# Mkdocs

On peut


[Fred Leleu_youtube](https://www.youtube.com/playlist?list=PL-Q7fIakgvUAcUluPeUMIP1128wWxboJY)
[https://ericecmorlaix.github.io/adn-Tutoriel_site_web/](https://ericecmorlaix.github.io/adn-Tutoriel_site_web/)



## Pluging
### Débuter avec MKdocs
[Débuter avec MKdocs - Forum NSI](https://mooc-forums.inria.fr/moocnsi/t/debuter-avec-mkdocs/1842/35)
[Débuter avec Mkdocs_Franck CHAMBON](https://ens-fr.gitlab.io/mkdocs/)
> [gitlab du site de franck Chambon](https://gitlab.com/ens-fr/mkdocs/)
[doc officielle de MKdocs matérial](https://squidfunk.github.io/mkdocs-material/)
https://www.youtube.com/channel/UC8rsmm4IE_F27RtXYtD_-rQ/videos

[MkDocs : Générer un site web pour votre documentation Markdown](https://www.youtube.com/watch?v=N3rwaXdeNNc)



### Autres trucs et astuces
[Truc et asstuces MKdocs - Forum NSI](https://mooc-forums.inria.fr/moocnsi/t/trucs-et-astuces-mkdocs/3629/3)
[madif taille de police dans les admonition forum](https://mooc-forums.inria.fr/moocnsi/t/resolu-mkdocs-changer-la-taille-de-police-des-collapsible-admonitions/6081/6)



#### Liens relatif à Mkdocs (site):
- [ens-fr](https://ens-fr.gitlab.io/mkdocs/)
- [ericecmorlaix](https://ericecmorlaix.github.io/adn-Tutoriel_site_web/MarkDown-Mkdocs_Material/)
- [squidfunk](https://squidfunk.github.io/mkdocs-material/)
- [bouillotvincent](https://bouillotvincent.gitlab.io/pyodide-mkdocs/)
- [bouillotvincent](https://bouillotvincent.gitlab.io/pyodide-mkdocs/todo/)
- [bouillotvincent](https://bouillotvincent.gitlab.io/pyodide-mkdocs/#installation)
- [ens-fr](https://ens-fr.gitlab.io/exp2/)
- [bouillotvincent.gitlab](https://bouillotvincent.gitlab.io/pyodide-mkdocs/)
- [bouillotvincent qcm mkdocs](https://mooc-forums.inria.fr/moocnsi/t/des-qcm-en-mkdocs/6004)
# Vidéo


## Example : 


<center><iframe width="560" height="315" src="https://www.youtube.com/embed/2LdLiCtVUEA?start=53&end=221" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center>

**code**

`<center><iframe width="560" height="315" src="https://www.youtube.com/embed/2LdLiCtVUEA?start=53&end=221" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center>`


width="560" :
height="315" :
src="https://www.youtube.com/embed/2LdLiCtVUEA?start=53&end=221" :
start=53&end=221 : les temps sont donnée en seconde depuis le début de la vidéo  d'origine.


## Ressources
# Console et notebook Basthon

## Notebook
### Exemple
<center><iframe width="1800" height="1800" src="https://notebook.basthon.fr/?from=examples/decorateurs.ipynb" title="basthon notebook" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center>

### Utilisation
Pour utiliser un netbook basthon dans un document mkdocs, il suffit d'introduire le code html suivant à l'endroit ou vous voulez le netbook.

```html
    <iframe width="1800" height="1800" src="https://notebook.basthon.fr/?from=examples/decorateurs.ipynb" title="basthon notebook" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
```

Pour précharger un document existant, il fait metter son adresse url après le `from` qui se trouve dan `src=`

> Commentaire : il faut complèter cette partie en regardant davantage la documentation de basthon.

<br>

## Console et Ide
Cette fonctionnalité est moins utile car il existe déjà un moyen d'avoir un ide et une console python dans Mkdocs, mais on ne sait jamais.

### Exemple
### Utilisation

<br>

## Ressources
[Baston](https://basthon.fr/)


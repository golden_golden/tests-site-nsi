# SQL

## 1. Le plugin Mkdocs-SQLite-Console
Mkdocs-SQLite-Console est un plugin pour MkDocs, qui permet d'afficher un IDE SQL (SQLite) permettant d'exécuter du code.


## 2. Exemple
{!{ sqlide titre="IDE avec initialisation et code pré-saisi" init="0_Mkdocs/sql/init1.sql" sql="0_Mkdocs/sql/code.sql" }!}

Affichage de l'IDE dans les admonitions qui sont repliées

??? bug
    Les blocs ne s'affichent pas correctement dans les admonitions qui sont repliées par défaut.

    {!{ sqlide titre="Init + Code" init="0_Mkdocs/sql/init1.sql" sql="0_Mkdocs/sql/code.sql" }!}
    
    Il faut cliquer sur le champ de texte pour que l'IDE s'affiche *(incorrectement)*.


## 3. Installation et activation du plugin Mkdocs-SQLite-Console

Si vous utilisez l'image docker associée à ce site et le dépot de basse de ce site tout est installé et configurée ! La note ci-dessous vous est donc inutile.

??? note "Installation et activation du plugin Mkdocs-SQLite-Console"
    Les informations ci-dessous ne sont utiles et nécéssaires que pour une installation à partir de zéro.
   
    ## 1. Installations le plugin dans python

    ### A. Installer le plugin avec pip :

    `pyhton3 -m pip install git+https://github.com/Epithumia/mkdocs-sqlite-console.git`

    ### B. Accéder à la documentation :

    ```shell
    git clone git+https://github.com/Epithumia/mkdocs-sqlite-console.git
    cd mkdocs-sqlite-console
    pyhton3 -m pip install .[docs]
    mkdocs build
    ```

    ## 2. Activation du plugin

    Dans votre fichier `mkdocs.yml`, ajouter:
    ```yaml
    plugins:
    - search
    - sqlite-console
    ```

    ??? note "Si vous n'aviez pas la section 'plugins'"
        Si vous n'avez aucune entrée dans la section `plugins` de votre fichier de configuration, vous voudrez sans doute ajouter le plugin `search`. MkDocs l'active par défaut s'il n'y a pas d'autres `plugins`, et dans le cas contraire, MkDocs demande de l'activer explicitement.


    ??? error "site_url"
        Si vous voulez déployer votre site (à l'aide de `mkdocs build` ou `mkdocs gh-deploy`), il faut également ajouter à votre fichier `mkdocs.yml` une ligne du type

        ```yaml
        site_url: https://monsite.url/chemin
        ```
        Si vous n'avez pas défini la variable `site_url` dans votre fichier `mkdocs.yml`, les commandes `mkdocs build` et `mkdocs gh-deploy` ne fonctionneront pas et signaleront la nécessité de le faire.


## 4. Bug et erreurs
??? note "Bug et erreurs"
    L'activation du plugin sql dans le document `mkdocs.yml` à pour conséquences de faire disparaitre les images conçu avec le plugin Graphviz.



## 5. Ressources

??? note "Ressources"
    - La totalité des informations de cette page à été prise sur la documentation officielle du plugin que vous trouverez : [Mkdocs-SQLite-console - Site officiel](https://epithumia.github.io/mkdocs-sqlite-console/)

    - La decouverte de ce plugin est liée à un fil sur le forum des enseignants de NSI que vous retouverez : [Un nouveau plugin pour MKdocs - Forum NSI](https://mooc-forums.inria.fr/moocnsi/t/un-nouveau-plugin-pour-mkdocs-mkdocs-sqlite-console/5973/49)


À traiter
[https://mooc-forums.inria.fr/moocnsi/t/un-nouveau-plugin-pour-mkdocs-mkdocs-sqlite-console/5973/51](https://mooc-forums.inria.fr/moocnsi/t/un-nouveau-plugin-pour-mkdocs-mkdocs-sqlite-console/5973/51)

[https://mooc-forums.inria.fr/moocnsi/t/sources-md-bac-sql/6203](https://mooc-forums.inria.fr/moocnsi/t/sources-md-bac-sql/6203)
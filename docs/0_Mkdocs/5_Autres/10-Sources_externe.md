---
title: "Sources externe"

---

# Inclusions de sources externe à la page

## Intégration de fichiers externes

!!! info "Le cas simple"
    Pour donner le contenu du fichier `mkdocs.yml` qui est situé dans `docs/`, on entre :

    ````markdown
    ```yaml
    --8<---​ "mkdocs.yml"
    ```
    ````

!!! warning "Points techniques"
    - :warning: Il faut donner le chemin en partant de la racine du projet.
    - Le contenu du fichier est remplacé dans le code source Markdown avant interprétation et son indentation est respectée, même si l'intégration se fait dans un bloc. :warning: Ce n'est pas le cas si `--8<--- <exemple>` est inclus dans une macro, l'indentation n'est plus respectée. On l'utilisera donc en dehors de macros.

!!! tip "Inclure un affichage de script"
    On prendra l'exemple de l'affichage de `docs/scripts/exemple.py`

    !!! note "Entrée"
        ````markdown
        Affichage de `docs/scripts/exemple.py`

        ```python
        --8<--- "docs/scripts/exemple.py"   <"ne RIEN écrire ici">
        ```

        Une fonction de tri.
        ````
    
    !!! done "Rendu"
        Affichage de `docs/scripts/exemple.py`

        ```python
        --8<--- "docs/scripts/exemple.py"
        ```

        Une fonction de tri.
    
    :warning: Il ne faut **rien** écrire après le nom de fichier, **pas même d'espace** !


??? danger "Pour aller plus loin"
    [La documentation (_en_)](https://facelessuser.github.io/pymdown-extensions/extensions/snippets/)



??? note "sources"
    - [https://ens-fr.gitlab.io/mkdocs/markdown-mkdocs/#integration-de-fichiers-externes](https://ens-fr.gitlab.io/mkdocs/markdown-mkdocs/#integration-de-fichiers-externes)
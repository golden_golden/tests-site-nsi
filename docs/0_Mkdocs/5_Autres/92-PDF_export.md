# Export en PDF




## 1. Convertir le site un pdf
Malgré l'existance de plugin permettant d'effecturer cette tache, il à été choisit d'utiliser une pages de style css spéciale pour le print.
Ainsi pour obtenir un pdf, il suffit de se placer sur la page que l'on veux, d'utiliser la fonction print de son navigateur web, et de selectionner dans les imprimantes une sortie en pdf.

## 2. Un cours légèrement différent pour le print
### A. Pour que des éléments n'apparaissent pas lors de l'impression
### B. Pour que des éléments n'apparaissent pas sur le site mais à l'impression






## 3. Ressources
??? note "Ressources"
    - Relatif aux plugins mkdocs : [Pdfs à partir de fiches MKdocs - Forum NSI](https://mooc-forums.inria.fr/moocnsi/t/pdfs-a-partir-de-fiches-mkdocs/3332/2)

    - [Pdfs à partir de fiches MKdocs - Github](https://github.com/brospars/mkdocs-page-pdf)

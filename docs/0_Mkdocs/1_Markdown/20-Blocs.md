---
title: "Les Blocs"

---

# Les Blocs

[https://ens-fr.gitlab.io/mkdocs/markdown-bases/](https://ens-fr.gitlab.io/mkdocs/markdown-bases/)


## Bloc de code 
**Bloc de code incorporé**
=== "Rendu"
    Voici du `code incorporé.`
        
=== "Code"
    ```Markdown
    Voici du `code incorporé.`
    ```



**Bloc de code avec annotations**
**Bloc de code incorporé**
=== "Rendu"
    ``` py hl_lines="2 3"
    def bubble_sort(items):
        for i in range(len(items)):
            for j in range(len(items) - 1 - i):
                if items[j] > items[j + 1]:
                    items[j], items[j + 1] = items[j + 1], items[j]
    ```

        
=== "Code"
    ```Markdown
        ``` py hl_lines="2 3"
        def bubble_sort(items):
            for i in range(len(items)):
                for j in range(len(items) - 1 - i):
                    if items[j] > items[j + 1]:
                        items[j], items[j + 1] = items[j + 1], items[j]
        ```
    ```

## Admonitions
### Différents types d'admonitions
=== "Rendu"
    !!! note
        texte dans le bloc

    !!! abstract "abstract, summary, tldr"
        texte dans le bloc

    !!! info "info, todo"
        texte dans le bloc

    !!! important "tip, hint, important"
        texte dans le bloc

    !!! check "success, check, done"
        texte dans le bloc

    !!! faq "question, help, faq"
        texte dans le bloc

    !!! attention "warning, caution, attention"
        texte dans le bloc

    !!! fail "failure, fail, missing"
        texte dans le bloc

    !!! danger "danger, error"
        texte dans le bloc

    !!! bug
        texte dans le bloc

    !!! example
        texte dans le bloc

    !!! quote "quote, cite"
        texte dans le bloc

        
=== "Code"
    ```Markdown
    !!! note
        texte dans le bloc

    !!! abstract "abstract, summary, tldr"
        texte dans le bloc

    !!! info "info, todo"
        texte dans le bloc

    !!! important "tip, hint, important"
        texte dans le bloc

    !!! check "success, check, done"
        texte dans le bloc

    !!! faq "question, help, faq"
        texte dans le bloc

    !!! attention "warning, caution, attention"
        texte dans le bloc

    !!! fail "failure, fail, missing"
        texte dans le bloc

    !!! danger "danger, error"
        texte dans le bloc

    !!! bug
        texte dans le bloc

    !!! example
        texte dans le bloc

    !!! quote "quote, cite"
        texte dans le bloc
    ```


### Différents comportements des admonitions
=== "Rendu"
    ???+ note "Admonition note"
        texte dans le bloc

        
=== "Code"
    ```Markdown
        ???+ note "Admonition note"
            texte dans le bloc
    ```

## Les citations

Pour insérer une citation, on utilise le chevron > devant chaque ligne de code :

=== "Rendu"
    > Voici une citation

    > sur plusieurs lignes.

=== "Code"
    ```markdown
    > Voici une citation

    > sur plusieurs lignes.
    ```


A noter que si on ne saute pas de ligne, MkDocs formate selon la longueur de la ligne courante : 

Pour insérer une citation, on utilise le chevron > devant chaque ligne de code :

=== "Rendu"
    > Voici une citation
    > sur une seule ligne.

=== "Code"
    ```markdown
    > Voici une citation
    > sur une seule ligne.
    ```



## Séparer des blocs
Quelque soit le nombre de ligne sauter entre deux blocs  identiques il arrive que ceux ci soit fusionner. 

Sans séparation 
=== "Rendu"
    === "Bloc 1"
        texte bloc 1

    === "Code bloc 1"
        code bloc 1


    === "Bloc 2"
        texte bloc 2

    === "Code bloc 2"
        code bloc 2


=== "Code"
    ````markdown
        === "Bloc 1"
            texte bloc 1

        === "Code bloc 1"
            code bloc 1


        === "Bloc 2"
            texte bloc 2

        === "Code bloc 2"
            code bloc 2
    ````





Avec séparation 
=== "Rendu"
    === "Bloc 1"
        texte bloc 1

    === "Code bloc 1"
        code bloc 1

    <br/>

    === "Bloc 2"
        texte bloc 2

    === "Code bloc 2"
        code bloc 2


=== "Code"
    ````markdown
        === "Bloc 1"
            texte bloc 1

        === "Code bloc 1"
            code bloc 1

        <br/>

        === "Bloc 2"
            texte bloc 2

        === "Code bloc 2"
            code bloc 2
    ````









## Ressources
??? note "Ressources"
    [https://www.zonensi.fr/Miscellanees/mkdocs_cmd/](https://www.zonensi.fr/Miscellanees/mkdocs_cmd/)

    [https://ericecmorlaix.github.io/adn-Tutoriel_site_web/MarkDown-Mkdocs_Material/](https://ericecmorlaix.github.io/adn-Tutoriel_site_web/MarkDown-Mkdocs_Material/)



    [https://okonore.github.io/languages_programmation/](https://okonore.github.io/languages_programmation/)



    Pour de plus amples informations, on pourra se référer à la page dédiée : <https://ens-fr.gitlab.io/mkdocs/maths/>





    [https://arminreiter.com/wp-content/uploads/2020/04/Markdown-Cheatsheet.pdf](https://arminreiter.com/wp-content/uploads/2020/04/Markdown-Cheatsheet.pdf)
    [https://www.collectiveray.com/fr/aide-m%C3%A9moire-de-d%C3%A9marque](https://www.collectiveray.com/fr/aide-m%C3%A9moire-de-d%C3%A9marque)
    [https://cheatography.com/lucbpz/cheat-sheets/the-ultimate-markdown/](https://cheatography.com/lucbpz/cheat-sheets/the-ultimate-markdown/)
    [https://enterprise.github.com/downloads/en/markdown-cheatsheet.pdf](https://enterprise.github.com/downloads/en/markdown-cheatsheet.pdf)
    [https://cheatography.com/lucbpz/cheat-sheets/the-ultimate-markdown/](https://cheatography.com/lucbpz/cheat-sheets/the-ultimate-markdown/)



    [https://blog.wax-o.com/2014/04/tutoriel-un-guide-pour-bien-commencer-avec-markdown/](https://blog.wax-o.com/2014/04/tutoriel-un-guide-pour-bien-commencer-avec-markdown/)
    [https://www.markdownguide.org/cheat-sheet/](https://www.markdownguide.org/cheat-sheet/)
    [https://www.markdownguide.org/basic-syntax](https://www.markdownguide.org/basic-syntax)
    [https://paperhive.org/help/markdown#paragraph](https://paperhive.org/help/markdown#paragraph)
    [https://markdown-guide.readthedocs.io/en/latest/basics.html](https://markdown-guide.readthedocs.io/en/latest/basics.html)



    - [https://ens-fr.gitlab.io/experience/aide-m%C3%A9moire/#les-boutons](https://ens-fr.gitlab.io/experience/aide-m%C3%A9moire/#les-boutons)
    - [https://gitlab.com/ens-fr/experience/](https://gitlab.com/ens-fr/experience/)




-------

--- À traiter



### Citations

!!! tip "Avec un chevron"
    Suivant l'ancien modèle sur les mails en mode texte, quand on répond à un message, le texte est décalé et chaque ligne est précédée d'un chevron `>`.

    === "Exemple simple"
        !!! note "Entrée"

            ```markdown
            > **Cookie** :
            >
            > - Anciennement petit gâteau sucré, qu'on acceptait avec plaisir.
            > - Aujourd'hui : petit fichier informatique drôlement salé, qu'il faut refuser avec véhémence.

            De Luc Fayard, _Dictionnaire impertinent des branchés_
            ```
            
        !!! done "Rendu"
            > **Cookie** :
            >
            > - Anciennement petit gâteau sucré, qu'on acceptait avec plaisir.
            > - Aujourd'hui : petit fichier informatique drôlement salé, qu'il faut refuser avec véhémence.

            De Luc Fayard, _Dictionnaire impertinent des branchés_

    === "Citations imbriquées"
        Voici un example de mail qu'on peut recevoir, témoin d'un échange. Quand un mail est long, on peut avoir envie de répondre dans la partie associée et non tout d'un bloc au départ.
        
        L'un dit « Salut ; À+», l'autre dit « Bonjour ; Merci ».

        !!! note "Entrée"

            ```markdown
            Salut, c'est bon pour dimanche, pour le reste je réponds dans le corps du mail.
            À+
            > Bonjour,  
            > un long blabla...  
            > avec une question pour dimanche.  
            > Et une autre question_1.  

            Réponse à question_1.  

            > Sinon blabla, et question_2.  

            Réponse à question_2.  

            > > Salut,  
            > > Tu fais quoi dimanche ?  
            > > À+  
            
            > Alors, je pense que...  
            > Merci et bonne journée  
            ```
            
        !!! done "Rendu"
            Salut, c'est bon pour dimanche, pour le reste je réponds dans le corps du mail.
            À+
            > Bonjour,  
            > un long blabla...  
            > avec une question pour dimanche.  
            > Et une autre question_1.  

            Réponse à question_1.  

            > Sinon blabla, et question_2.  

            Réponse à question_2.  

            > > Salut,  
            > > Tu fais quoi dimanche ?  
            > > À+  

            > Alors, je pense que...  
            > Merci et bonne journée  

### Code en bloc

!!! tip "En indentant le bloc, 4 espaces"
    La méthode simple pour qu'un extrait ne soit pas formaté est de décaler le bloc de 4 espaces ou plus. On saute également une ligne avant et après, comme pour un paragraphe.

    === "Correct"
        !!! note "Entrée"

            ```markdown
            Voici un code Python

                n = 47**2 + 31**2
            ```

        !!! done "Rendu"
            Voici un code Python

                n = 47**2 + 31**2

    === "Incorrect"
        !!! note "Entrée"

            ```markdown
            Voici un code Python

            n = 47**2 + 31**2
            ```

        !!! fail "Rendu"
            Voici un code Python

            n = 47**2 + 31**2
        
        :warning: Ici le moteur a interprété une partie avec de l'emphase forte.

!!! info "Et la coloration syntaxique ?"
    Nous verrons cela dans [une partie de Markdown avancé](#coloration-syntaxique-du-code).

### Code en ligne

!!! tip "Avec l'accent grave"
    Avec un clavier azerty, ++altgr+7++ donne l'[accent grave](https://fr.wikipedia.org/wiki/Accent_grave) `` ` ``

    On utilise la syntaxe : ``Du texte avec un `identifiant` de code``.

    === "Correct"
        !!! note "Entrée"

            ```markdown
            La définition de la fonction `premier` commence avec le mot clé `def`

            Elle prend en paramètre un entier `n`

            Elle renvoie un booléen avec le mot clé `return`
            ```

        !!! done "Rendu"
            La définition de la fonction `premier` commence avec le mot clé `def`

            Elle prend en paramètre un entier `n`

            Elle renvoie un booléen avec le mot clé `return`
    
    === "Incorrect"
        !!! note "Entrée"

            ```markdown
            La définition de la fonction _premier_ commence avec le mot clé **def**

            Elle prend en paramètre un entier _n_

            Elle renvoie un booléen avec le mot clé **return**
            ```

        !!! fail "Rendu"
            La définition de la fonction _premier_ commence avec le mot clé **def**

            Elle prend en paramètre un entier _n_

            Elle renvoie un booléen avec le mot clé **return**

        :warning: Les effets de texte ne servent pas à désigner des bouts de code.
    
    === "Le futur avec MkDocs"
        !!! note "Entrée"

            ```markdown
            La définition de la fonction `#!py3 premier` commence avec le mot clé `#!py3 def`

            Elle prend en paramètre un entier `#!py3 n`

            Elle renvoie un booléen avec le mot clé `#!py3 return`
            ```

        !!! done "Rendu"
            La définition de la fonction `#!python premier` commence avec le mot clé `#!python def`

            Elle prend en paramètre un entier `#!python n`

            Elle renvoie un booléen avec le mot clé `#!python return`

        Ceci n'est possible qu'avec MkDocs, et des extensions, on peut donc avoir la coloration syntaxique en ligne avec des langages variés. **Nous y reviendrons plus tard.**

    !!! warning "Technique avancée"
        Si on souhaite écrire du code en ligne qui contient des `` ` ``,
         il suffit d'encadrer le morceau avec plus de `` ` `` qu'il n'y en a consécutivement dans le morceau.
        === "Exemple simple correct"
            !!! note "Entrée"
                ```markdown
                Afficher **du code** avec accent grave : `` un entier `n` ``
                ```

            !!! done "Rendu"
                Afficher **du code** avec accent grave : `` un entier `n` ``

        === "Exemple simple incorrect"
            !!! note "Entrée"
                ```markdown
                Afficher **du code** avec accent grave : `un entier `n``
                ```

            !!! done "Rendu"
                Afficher **du code** avec accent grave : `un entier `n``
            
            :warning: Comment savoir où est la fin et le début ?

        === "Exemple élaboré"
            !!! note "Entrée"
                ```markdown
                - Afficher 1`` ` `` : `` ` ``
                - Afficher  2`` ` `` : ``` `` ```
                - Afficher le code précédent : ```` - Afficher  2`` ` `` : ``` `` ``` ````
                ```

            !!! done "Rendu"
                - Afficher 1`` ` `` : `` ` ``
                - Afficher  2`` ` `` : ``` `` ```
                - Afficher le code précédent : ```` - Afficher  2`` ` `` : ``` `` ``` ````






------


## Les admonitions

!!! info "Pourquoi ?"
    Elles permettent de délimiter des blocs de contenu
      avec une touche de couleur, sans créer de nouvelles
      entrées dans la table des matières.
    
    Elles structurent donc sans alourdir les onglets de navigation.

!!! example "Exemple"
    !!! note "Entrée"
        ```markdown
        !!! info "Pourquoi ?"
            Elles permettent de délimiter des blocs de contenu
             avec une touche de couleur, sans créer de nouvelles
             entrées dans la table des matières.
            
            Elles structurent donc sans alourdir les onglets de navigation.
        ```
    !!! done "Rendu"
        !!! info "Pourquoi ?"
            Elles permettent de délimiter des blocs de contenu
             avec une touche de couleur, sans créer de nouvelles
             entrées dans la table des matières.
            
            Elles structurent donc sans alourdir les onglets de navigation.

    👍 Il s'agit du code pour produire le paragraphe précédent !

!!! faq "Comment l'avoir enroulée, à dérouler ?"
    En remplaçant `!!!` par `???`. Cela correspond à la balise `<details>` en HTML.

    Comme l'exemple d'introduction qui contient le fichier `mkdocs.yml`.

    `???+` permet de l'avoir déroulée à l'ouverture de la page, et qu'elle
    puisse être ensuite enroulée.

!!! example "Les types de boites"
    === "`note`"
        !!! note "Pour une note"
            `note` ou `seealso`
            ````markdown
            ```markdown
            !!! note "Pour une note"
                `note` ou `seealso`
            ```
            ````

    === "`tldr`"
        !!! tldr "Pour un résumé"
            `tldr`, `summary` ou `abstract`
            ````markdown
            ```markdown
            !!! tldr "Pour un résumé"
                `tldr`, `summary` ou `abstract`
            ```
            ````

    === "`info`"
        !!! info "Pour une information"
            `info` ou `todo`
            ````markdown
            ```markdown
            !!! info "Pour une information"
                `info` ou `todo`
            ```
            ````

    === "`tip`"
        !!! tip "Pour une astuce"
            `tip`, `hint` ou `important`
            ````markdown
            ```markdown
            !!! tip "Pour une astuce"
                `tip`, `hint` ou `important`
            ```
            ````

    === "`done`"
        !!! done "Pour une réussite"
            `done`, `check` ou `success`
            ````markdown
            ```markdown
            !!! done "Pour une réussite"
                `done`, `check` ou `success`
            ```
            ````

    === "`faq`"
        !!! faq "Pour une question"
            `faq`, `help` ou `question`
            ````markdown
            ```markdown
            !!! faq "Pour une question"
                `faq`, `help` ou `question`
            ```
            ````

    === "`warning`"
        !!! warning "Pour une difficulté"
            `warning`, `caution` ou `attention`
            ````markdown
            ```markdown
            !!! warning "Pour une difficulté"
                `warning`, `caution` ou `attention`
            ```
            ````

    === "`fail`"
        !!! fail "Pour un échec"
            `fail`, `failure` ou `missing`
            ````markdown
            ```markdown
            !!! fail "Pour un échec"
                `fail`, `failure` ou `missing`
            ```
            ````

    === "`danger`"
        !!! danger "Pour un danger"
            `danger` ou `error`
            ````markdown
            ```markdown
            !!! danger "Pour un danger"
                `danger` ou `error`
            ```
            ````

    === "`bug`"
        !!! bug "Pour un bogue"
            `bug`
            ````markdown
            ```markdown
            !!! bug "Pour un bogue"
                `bug`
            ```
            ````

    === "`example`"
        !!! example "Pour des exemples"
            `example`
            ````markdown
            ```markdown
            !!! example "Pour des exemples"
                `example`
            ```
            ````

    === "`cite`"
        !!! cite "Pour une citation"
            `cite`
            ````markdown
            ```markdown
            !!! cite "Pour une citation"
                `cite`
            ```
            ````

    On verra dans les recettes élaborées comment fabriquer ses propres boîtes, couleur et icône.


## Options sur les blocs de code

!!! tip "Numérotation des lignes et marquage"
    - Il suffit d'ajouter `linenums="1"` (ou un autre nombre) pour faire débuter la numérotation.
    - Pour marquer des lignes en particulier, on utilise `hl_lines="<tranches et numéros>"`


!!! example "Exemples"
    === "Sans numérotation"
        !!! note "Entrée"
            ````markdown
            ```python
            --8<--- "docs/scripts/exemple.py"
            ```
            ````
        
        !!! done "Rendu"
            ```python
            --8<--- "docs/scripts/exemple.py"
            ```

    === "Numérotation classique"
        !!! note "Entrée"
            ````markdown
            ```python linenums="1"
            --8<--- "docs/scripts/exemple.py"
            ```
            ````
        
        !!! done "Rendu"
            ```python linenums="1"
            --8<--- "docs/scripts/exemple.py"
            ```

    === "Numérotation décalée"
        !!! note "Entrée"
            ````markdown
            ```python linenums="42"
            --8<--- "docs/scripts/exemple.py"
            ```
            ````
        
        !!! done "Rendu"
            ```python linenums="42"
            --8<--- "docs/scripts/exemple.py"
            ```

    === "Marquage d'une ligne"
        !!! note "Entrée"
            ````markdown
            ```python hl_lines="2"
            --8<--- "docs/scripts/exemple.py"
            ```
            ````
        
        !!! done "Rendu"
            ```python hl_lines="2"
            --8<--- "docs/scripts/exemple.py"
            ```

    === "Marquage d'une ligne, avec numérotation"
        !!! note "Entrée"
            ````markdown
            ```python linenums="1" hl_lines="2"
            --8<--- "docs/scripts/exemple.py"
            ```
            ````
        
        !!! done "Rendu"
            ```python linenums="1" hl_lines="2"
            --8<--- "docs/scripts/exemple.py"
            ```

    === "Marquage de lignes éparses"
        !!! note "Entrée"
            ````markdown
            ```python linenums="1" hl_lines="2 5"
            --8<--- "docs/scripts/exemple.py"
            ```
            ````
        
        !!! done "Rendu"
            ```python linenums="1" hl_lines="2 5"
            --8<--- "docs/scripts/exemple.py"
            ```

    === "Marquage d'une tranche"
        !!! note "Entrée"
            ````markdown
            ```python linenums="1" hl_lines="2-4"
            --8<--- "docs/scripts/exemple.py"
            ```
            ````
        
        !!! done "Rendu"
            ```python linenums="1" hl_lines="2-4"
            --8<--- "docs/scripts/exemple.py"
            ```

    === "Marquage lignes et tranches"
        !!! note "Entrée"
            ````markdown
            ```python linenums="1" hl_lines="1 2 3-3 5-5"
            --8<--- "docs/scripts/exemple.py"
            ```
            ````
        
        !!! done "Rendu"
            ```python linenums="1" hl_lines="1 2 3-3 5-5"
            --8<--- "docs/scripts/exemple.py"
            ```

??? bug "Bug ???"
    1. J'ai constaté des ratés de coloration syntaxique avec ```` ``` c++ ````.

        - Recommendations :
            - Pas d'espace entre ```` ``` ```` et le nom du langage.
            - Utiliser `cpp` à la place de `c++`

    2. Ne pas choisir l'option globale `linenums: false` dans `mkdocs.yml`
        - On ne peut plus la réactiver ensuite
        - On peut toutefois choisir `linenums: true`, (mais sans la mettre en pause ???)



## Les panneaux coulissant (*SuperFences*)

!!! example "Exemple simple"
    !!! note "Entrée"

        ```` markdown

        === "`C`"

            ```c
            #include <stdio.h>

            int main(void) {
              printf("Hello world!\n");
              return 0;
            }
            ```

        === "`C++`"

            ```cpp
            #include <iostream>

            int main(void) {
              std::cout << "Hello world!" << std::endl;
              return 0;
            }
            ```

        ````

    !!! done "Rendu"

        === "C"

            ```c
            #include <stdio.h>

            int main(void) {
              printf("Hello world!\n");
              return 0;
            }
            ```

        === "C++"

            ```cpp
            #include <iostream>

            int main(void) {
              std::cout << "Hello world!" << std::endl;
              return 0;
            }
            ```

??? danger "Exemple élaboré"
    !!! note "Entrée"

        ```` markdown

        === "Liste non numérotée"

            _Exemple_ :

            ``` markdown
            - Sed sagittis eleifend rutrum
            - Donec vitae suscipit est
            - Nulla tempor lobortis orci
            ```

            _Résultat_ :

            - Sed sagittis eleifend rutrum
            - Donec vitae suscipit est
            - Nulla tempor lobortis orci

        === "Liste numérotée"

            _Exemple_ :

            ``` markdown
            1. Sed sagittis eleifend rutrum
            2. Donec vitae suscipit est
            3. Nulla tempor lobortis orci
            ```

            _Résultat_ :

            1. Sed sagittis eleifend rutrum
            2. Donec vitae suscipit est
            3. Nulla tempor lobortis orci

        ````

    !!! done "Rendu"

        === "Liste non numérotée"

            _Exemple_ :

            ``` markdown
            - Sed sagittis eleifend rutrum
            - Donec vitae suscipit est
            - Nulla tempor lobortis orci
            ```

            _Résultat_ :

            - Sed sagittis eleifend rutrum
            - Donec vitae suscipit est
            - Nulla tempor lobortis orci

        === "Liste numérotée"

            _Exemple_ :

            ``` markdown
            1. Sed sagittis eleifend rutrum
            2. Donec vitae suscipit est
            3. Nulla tempor lobortis orci
            ```

            _Résultat_ :

            1. Sed sagittis eleifend rutrum
            2. Donec vitae suscipit est
            3. Nulla tempor lobortis orci



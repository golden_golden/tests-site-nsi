---
title: "Autres"

---

# Autres

## Bouton
## Les boutons

Un lien cliquable peut être transformé en bouton en lui adjoignant un élément de CSS `{ .md-button }`

!!! example "Exemples"
    === "Rectangle creux"
        !!! note "Entrée"
            ```markdown
            [Une console Basthon](https://console.basthon.fr/){ .md-button }
            ```
        !!! done "Rendu"
            [Une console Basthon](https://console.basthon.fr/){ .md-button }

    === "Rectangle plein"
        !!! note "Entrée"
            ```markdown
            [Une console Basthon](https://console.basthon.fr/){ .md-button .md-button--primary }
            ```
        !!! done "Rendu"
            [Une console Basthon](https://console.basthon.fr/){ .md-button .md-button--primary }



## Notes de bas de pages

=== "Rendu"
    [^2]:
        Le texte de la note de bas de page est ici.

=== "Code"
    ```Markdown
        [^2]:
            Le texte de la note de bas de page est ici.
    ```




## Notes de bas de page

!!! tip "Avec `[^id]` et `[^id]: description`"
    On place une indication de note en bas de page avec `[^id]` où `id` est un identifiant.

!!! example "Exemple"
    !!! note "Entrée"
        ```markdown
        Lorem ipsum[^ip] dolor sit amet, consectetur adipiscing[^ad] elit.[^el]

        [^ip]: Lorem ipsum dolor sit amet, consectetur adipiscing elit.

        [^ad]:
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
            nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
            massa, nec semper lorem quam in massa.

        [^el]:
            Ce texte [Lorem ipsum ...](https://fr.wikipedia.org/wiki/Lorem_ipsum)
            est un faux-texte destiné à remplir la vide.
        ```

    !!! done "Rendu"
        Lorem ipsum[^ip] dolor sit amet, consectetur adipiscing[^ad] elit.[^el]

        [^ip]: Lorem ipsum dolor sit amet, consectetur adipiscing elit.

        [^ad]:
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
            nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
            massa, nec semper lorem quam in massa.

        [^el]:
            Ce texte [Lorem ipsum ...](https://fr.wikipedia.org/wiki/Lorem_ipsum)
            est un faux-texte destiné à remplir la vide.
    
    :warning: Les identifiants sont numérotés automatiquement par ordre d'arrivée.
    
    - Ici, la première note (`ip`) est accessible avec un lien local `#fn:ip`, comme [ici](#fn:ip).
    - Le retour ici se fait avec `#fnref:ip` comme [ceci](#fnref:ip).








---------

---À traiter

### Émojis et Unicode

!!! tip "🤪👩‍🎨🚦🚂⚔️"
    Il suffit de coller vos émojis préférés depuis un site.
    
    - [Emojipedia](https://emojipedia.org/)
    - [GetEmoji](https://getemoji.com/)
    - [Copy and Paste Emoji](https://www.copyandpasteemoji.com/)
    - [wpRock](https://wprock.fr/t/emoji/) ; avec recherche en français.
    - [emoji copy](https://www.emojicopy.com/) ; permet d'en copier plusieurs à la fois.
    - ...

De manière encore plus générale, il est possible d'insérer tout caractère Unicode dans le fichier source en Markdown.

!!! note "Entrée"
    ```markdown
    Le [copyleft](https://fr.wikipedia.org/wiki/Copyleft) (🄯), est l'autorisation donnée par l'auteur d'un travail soumis au droit d'auteur d'utiliser, d'étudier, de modifier et de diffuser son œuvre, dans la mesure où cette même autorisation reste préservée.

    Le caractère copyleft (🄯) a été ajouté au standard Unicode le 5 juin 2018 au titre de la version 11.0. Il a pour code U+1F12F.
    ```

!!! done "Rendu"
    Le [copyleft](https://fr.wikipedia.org/wiki/Copyleft) (🄯), est l'autorisation donnée par l'auteur d'un travail soumis au droit d'auteur d'utiliser, d'étudier, de modifier et de diffuser son œuvre, dans la mesure où cette même autorisation reste préservée.

    Le caractère copyleft (🄯) a été ajouté au standard Unicode le 5 juin 2018 au titre de la version 11.0. Il a pour code U+1F12F.

Remarque, c'est aussi valable en Python, avec par exemple, (merci @sebhoa)

```python
JEU_52 = (
    '🂡🂢🂣🂤🂥🂦🂧🂨🂩🂪🂫🂭🂮',
    '🂱🂲🂳🂴🂵🂶🂷🂸🂹🂺🂻🂽🂾',
    '🃁🃂🃃🃄🃅🃆🃇🃈🃉🃊🃋🃍🃎',
    '🃑🃒🃓🃔🃕🃖🃗🃘🃙🃚🃛🃝🃞',
    '🂠🂬🂼🃌🃜🃏🃟'
    )
```

:warning: Le rendu sur certains systèmes d'exploitation peut être incorrect.
 If faudra attendre une mise à jour proposée.

!!! faq "Où trouver les caractères Unicode ?"
    - On peut rechercher par catégorie sur [Charbase](https://charbase.com/block)
    - On peut aussi trouver de nombreux caractères sur
     [CharacterCodes](https://www.charactercodes.net/)
    - On peut dessiner un caractère et voir s'il existe sur
     [Shapecatcher](https://shapecatcher.com/)

## Exercices élaborés

### Exercice 1

???

### Exercice 2

???

----
## Affichage des touches

!!! example "Exemple 1"
        !!! note "Markdown"
            ```markdown
            ++ctrl+alt+del++
            ```
        !!! done "Rendu"
            ++ctrl+alt+del++

!!! example "Exemple 2"
        !!! note "Markdown"
            ```markdown
            ++"⇑ Maj."+"Entrée ↵"++
            ```
        !!! done "Rendu"
            ++"⇑ Maj."+"Entrée ↵"++
        
        On peut définir ses propres touches avec `"ma touche"`


La liste des touches disponibles est dans la [documentation](
    https://facelessuser.github.io/pymdown-extensions/extensions/keys/#key-map-index).





## Ressources
??? note "Ressources"
    - [https://www.zonensi.fr/Miscellanees/mkdocs_cmd/](https://www.zonensi.fr/Miscellanees/mkdocs_cmd/)

    - [https://ericecmorlaix.github.io/adn-Tutoriel_site_web/MarkDown-Mkdocs_Material/](https://ericecmorlaix.github.io/adn-Tutoriel_site_web/MarkDown-Mkdocs_Material/)



    - [https://okonore.github.io/languages_programmation/](https://okonore.github.io/languages_programmation/)



    - Pour de plus amples informations, on pourra se référer à la page dédiée : <https://ens-fr.gitlab.io/mkdocs/maths/>





    - [https://arminreiter.com/wp-content/uploads/2020/04/Markdown-Cheatsheet.pdf](https://arminreiter.com/wp-content/uploads/2020/04/Markdown-Cheatsheet.pdf)
    - [https://www.collectiveray.com/fr/aide-m%C3%A9moire-de-d%C3%A9marque](https://www.collectiveray.com/fr/aide-m%C3%A9moire-de-d%C3%A9marque)
    - [https://cheatography.com/lucbpz/cheat-sheets/the-ultimate-markdown/](https://cheatography.com/lucbpz/cheat-sheets/the-ultimate-markdown/)
    - [https://enterprise.github.com/downloads/en/markdown-cheatsheet.pdf](https://enterprise.github.com/downloads/en/markdown-cheatsheet.pdf)
    - [https://cheatography.com/lucbpz/cheat-sheets/the-ultimate-markdown/](https://cheatography.com/lucbpz/cheat-sheets/the-ultimate-markdown/)



    - [https://blog.wax-o.com/2014/04/tutoriel-un-guide-pour-bien-commencer-avec-markdown/](https://blog.wax-o.com/2014/04/tutoriel-un-guide-pour-bien-commencer-avec-markdown/)
    - [https://www.markdownguide.org/cheat-sheet/](https://www.markdownguide.org/cheat-sheet/)
    - [https://www.markdownguide.org/basic-syntax](https://www.markdownguide.org/basic-syntax)
    - [https://paperhive.org/help/markdown#paragraph](https://paperhive.org/help/markdown#paragraph)
    - [https://markdown-guide.readthedocs.io/en/latest/basics.html](https://markdown-guide.readthedocs.io/en/latest/basics.html)



    - [https://ens-fr.gitlab.io/experience/aide-m%C3%A9moire/#les-boutons](https://ens-fr.gitlab.io/experience/aide-m%C3%A9moire/#les-boutons)
    - [https://gitlab.com/ens-fr/experience/](https://gitlab.com/ens-fr/experience/)





## Lien sur les en-têtes

## Icônes, émojis

On peut utiliser un émoji de plusieurs façons.

1. En trouvant son caractère Unicode, voir [Émojis](https://ens-fr.gitlab.io/mkdocs/markdown-bases/#emojis-et-unicode).
2. En utilisant son `shortcode` que l'on peut trouver sur [emojipedia](https://emojipedia.org/)

!!! note "Markdown"
    ```markdown
    Je pars :surfer:
    ```
!!! note "Markdown"
    Je pars :surfer:

Il existe aussi des icônes qui sont dans le dossier d'installation de _Material for MkDocs_.

Ce dossier est probablement TODO

:octicons-heart-fill-24:{ .heart }



## Meta tags


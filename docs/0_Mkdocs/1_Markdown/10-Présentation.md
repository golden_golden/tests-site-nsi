---
title: "MarkDown"

---


---- À traiter 


# Présentation du Markdown


## Qu'est ce que le Markdown
[Markdown](https://fr.wikipedia.org/wiki/Markdown) est un langage de balisage léger qui est traduit en HTML, langage que le navigateur est capable de lire pour produire une page web.


Il ne faut que **5 minutes pour avoir les bases** suffisantes.


## Utilisation 

On peut utiliser la syntaxe Markdown dans de nombreuses situations de travail collaboratif.

- Dans des forums, comme Discourse, **GitLab**, Reddit, Qt, Stack Overflow et Stack Exchange parmi d'autres.
- Dans des logiciels, comme **CodiMD**, **Jupyter**, ou **MkDocs** parmi d'autres.

## Avantages

On peut aussi, à partir d'un fichier Markdown `.md`, produire un fichier `.pdf`, ou `.odt`, ou `.docx` en utilisant l'utilitaire [Pandoc](https://fr.wikipedia.org/wiki/Pandoc). Nous ne nous en servirons pas ici.




## Défauts


Soyons honnêtes, il y a un défaut, Markdown n'est pas unifié.

- Il existe plusieurs variantes légèrement différentes de Markdown.
- Pour chaque variante, il peut exister des moteurs de rendus légèrement différents.

Nous utiliserons ici la **partie commune à toutes les versions**, avec le style le plus commun possible, ce sera en particulier utile pour travailler avec **Jupyter**.
    
Dans la partie suivante nous travaillerons avec certaines facilités offertes par **MkDocs**.



??? note "Ressources : Voir d'autres tutoriels"
    === "En anglais"
        - [Markdown Guide](https://www.markdownguide.org/) ; assez complet.
        - [Markdown Tutorial](https://www.markdowntutorial.com/) ;
          avec exercices, (multilingue, mais ...)
        - [Tutorial.md](http://agea.github.io/tutorial.md/) ;
          interactif, mérite une traduction !

    === "En français"
        - [Élaboration et conversion de documents avec Markdown et Pandoc](
            https://enacit1.epfl.ch/markdown-pandoc/) ;
            pour les utilisateurs avancés.



??? note "Sources"
    - Franck CHAMBON [https://ens-fr.gitlab.io/mkdocs/](https://ens-fr.gitlab.io/mkdocs/)
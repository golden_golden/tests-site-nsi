---
title: "Maths"

---

# Écrire des Mathématiques

On peut écrire des maths dans mkdocs à l'aide du languages $\LaTeX$.

$$\sum_{k=1}^{\infty} \frac{1}{k^2} = \frac{\pi^2}{6}$$




## 1. Intégration de $\LaTeX$ dans mkdocs
### 1.1 Des mathématiques dans le texte 

!!! note ""
    === "Rendu"
        On peut écrire du $\LaTeX$ au milieu d'une phrase comme ici  $1+5-3=3$  .
        
    === "Code"
        ```latex title="Formule TeX"
            On peut écrire du $\LaTeX$ au milieu d'une phrase comme ici  $1+5-3=3$  .
        ```

### 1.2 Des mathématiques hors d'un texte
!!! note ""
    === "Rendu"
        $$\sum_{k=1}^{\infty} \frac{1}{k^2} = \frac{\pi^2}{6}$$

    === "Code"
        ```latex title="Formule TeX"
        $$\sum_{k=1}^{\infty} \frac{1}{k^2} = \frac{\pi^2}{6}$$
        ```

## 2. Quelques fonctions $\LaTeX$

### 2.1 Opérateurs
!!! note ""
    === "Rendu"
        $$ à faire $$

    === "Code"
        ```latex title="Formule TeX"
        à faire 
        ```

### 2.2 Fonctions
!!! note ""
    === "Rendu"
        $$ à faire $$

    === "Code"
        ```latex title="Formule TeX"
        à faire 
        ```

### 2.3 Indices et exposants
!!! note ""
    === "Rendu"
        $a^n = \left(a^{n/2}\right)^2$  et $a^n = \left(a^{(n-1)/2}\right)^2×a$  et $f_{91}(n)$, pour $n\in [![0..100]!]$ et $a_n = a_{n-a_{n-1}} + a_{n-a_{n-2}}$, pour $n \geqslant 3$ et $n > 1$ et $S(n) = \sum\limits_{i=1}^n a_i$.

    === "Code"
        ```latex title="Formule TeX"
        $a^n = \left(a^{n/2}\right)^2$ et $a^n = \left(a^{(n-1)/2}\right)^2×a$ et $f_{91}(n)$, pour $n\in [![0..100]!]$ et $a_n = a_{n-a_{n-1}} + a_{n-a_{n-2}}$, pour $n \geqslant 3$ et $n > 1$ et $S(n) = \sum\limits_{i=1}^n a_i$.
        ```




### 2.4 Matrices
!!! note ""
    === "Rendu"
        $$\begin{pmatrix}
        0 & 1 & 0 & 1 \\
        1 & 0 & 1 & 1 \\
        1 & 0 & 0 & 0 \\
        1 & 1 & 1 & 0 \\
        \end{pmatrix}
        $$


    === "Code"
        ```latex title="Formule TeX"
        $$\begin{pmatrix}
        0 & 1 & 0 & 1 \\
        1 & 0 & 1 & 1 \\
        1 & 0 & 0 & 0 \\
        1 & 1 & 1 & 0 \\
        \end{pmatrix}
        $$
        ```

### 2.5 Lettres greques
!!! note ""
    === "Rendu"
        $$\Gamma\ \Delta\ \Theta\ \Lambda\ \Xi\ \Pi\ \Sigma\ \Upsilon\ \Phi\ \Psi\ \Omega$$

    === "Code"
        ```latex title="Formule TeX"
        $$\Gamma\ \Delta\ \Theta\ \Lambda\ \Xi\ \Pi\ \Sigma\ \Upsilon\ \Phi\ \Psi\ \Omega$$
        ```


!!! note ""
    === "Rendu"
        $$\alpha\ \beta\ \gamma\ \delta\ \epsilon\ \zeta\ \eta\ \theta\ \iota\ \kappa\ \lambda\ \mu\ \nu\ \xi$$

    === "Code"
        ```latex title="Formule TeX"
        $$\alpha\ \beta\ \gamma\ \delta\ \epsilon\ \zeta\ \eta\ \theta\ \iota\ \kappa\ \lambda\ \mu\ \nu\ \xi$$
        ```

### 2.6 Flèches
!!! note ""
    === "Rendu"
        $$ à faire $$

    === "Code"
        ```latex title="Formule TeX"
        à faire 
        ```

### 2.7 Ensemble
!!! note ""
    === "Rendu"
        $$ à faire $$

    === "Code"
        ```latex title="Formule TeX"
        à faire 
        ```

## 3. Ressources
??? note "Ressources"
    ---- à traiter
    - [https://ens-fr.gitlab.io/mkdocs/maths/](https://ens-fr.gitlab.io/mkdocs/maths/)
    
    -----

    - [Aide formules Tex](https://fr.wikipedia.org/wiki/Aide:Formules_TeX)

    - Pour de plus amples informations, on pourra se référer à la page dédiée : [https://ens-fr.gitlab.io/mkdocs/maths/](https://ens-fr.gitlab.io/mkdocs/maths/)


    - [intmath](https://www.intmath.com/cg5/katex-mathjax-comparison.php)

    - [https://fr.wikibooks.org/wiki/LaTeX/%C3%89crire_des_math%C3%A9matiques](https://fr.wikibooks.org/wiki/LaTeX/%C3%89crire_des_math%C3%A9matiques)

    - [https://c-tan.com/post/latex-math-cheat-sheet/](https://c-tan.com/post/latex-math-cheat-sheet/)

    ** traiter les maths avec un plugin de python markdown** 

    - [https://pypi.org/project/markdown-katex/](https://pypi.org/project/markdown-katex/)





----
---À traiter

## Utilisation de MathJax

Suivant les indications de la documentation :

- [MkDocs Matertial - MathJax](https://squidfunk.github.io/mkdocs-material/reference/mathjax/)
- [arithmatex](https://facelessuser.github.io/pymdown-extensions/extensions/arithmatex/)
- [faceless `mkdocs.yml`](https://github.com/facelessuser/pymdown-extensions/blob/main/mkdocs.yml)

1. On modifie le fichier `mkdocs.yml` :

```yaml
markdown_extensions:
    - pymdownx.arithmatex:          # les maths
        generic: true               #    avec $ et $$

extra_javascript:
    - xtra/javascripts/mathjax-config.js                    # MathJax
    - https://polyfill.io/v3/polyfill.min.js?features=es6
    - https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js

extra_css:
    - xtra/stylesheets/ajustements.css                      # ajustements
```

2. On crée un dossier `docs/xtra/javascripts` dans lequel

    - on place le fichier `mathjax-config.js`

    ```js
        window.MathJax = {
        tex: {
            inlineMath: [["\\(", "\\)"]],
            displayMath: [["\\[", "\\]"]],
            processEscapes: true,
            processEnvironments: true
        },
        options: {
            ignoreHtmlClass: ".*|",
            processHtmlClass: "arithmatex"
        }
        };

        document$.subscribe(() => {
        MathJax.typesetPromise()
        })
    ```

3. On crée un dossier `docs/xtra/stylesheets` dans lequel

    - on place le fichier `ajustements.css` ; merci @sebhoa

    ```js
    .md-typeset div.arithmatex  {
        overflow: initial;
    }

    .md-typeset .admonition {
        font-size: 0.8rem;
    }

    .md-typeset details {
        font-size: 0.8rem;
    }
    ```




---
title: "Texte"

---


# Texte

[https://ens-fr.gitlab.io/mkdocs/markdown-bases/](https://ens-fr.gitlab.io/mkdocs/markdown-bases/)


## Les titres

=== "Rendu"
    # Titre de niveau H1
    ## Titre de niveau H2
    ### Titre de niveau H3

=== "Code"
    ````markdown
        # Titre de niveau H1
        ## Titre de niveau H2
        ### Titre de niveau H3
    ````


## Séparer des paragraphe

=== "Rendu"
    un texte ici 
    et un texte là

=== "Code"
    ````markdown
    un texte ici 
    et un texte là
    ````

<br/>

=== "Rendu"
    un texte ici

    et un texte là

=== "Code"
    ````markdown
    un texte ici

    et un texte là
    ````






## Emphase : italique, gras
=== "Rendu"
    *Texte en italique*

    **Texte en gras**


=== "Code"
    ````markdown
       *Texte en italique*
    
        **Texte en gras**
    ````

## Indices, exposants
Il exite deux possibilités : soit utilisé des balises `HTML`, soit utilsé `mathjax` mais le rendu sera différent.

----À traiter

=== "Rendu"
    - texte^exposant^
    - texte~indice~

=== "Code"
    ```markdown
      - texte^exposant^
      - texte~indice~
    ```
    
-----



### Avec du `HTML` 
=== "Rendu" 
    texte<sup>Exposant</sup>

    texte<sub>Indice</sub>

=== "Code"
    ````markdown
        texte<sup>Exposant</sup>

        texte<sub>Indice</sub>
    ````

<br/>

### Avec `Mathjax`
=== "Rendu"
    $texte^{exposant}$
    
    $texte_{indice}$

=== "Code"
    ````markdown
        $texte^{exposant}$
        
        $texte_{indice}$
    ````





## Souligner, rayer, surligner
=== "Rendu"
    ^^texte souligner^^

    ~~texte rayer~~

    ==texte surligner==



=== "Code"
    ````markdown
        ^^texte souligner^^
    
        ~~texte rayer~~

        ==texte surligner==
    ````



## Listes
### ordonée

=== "Rendu"
        1. First item
        5. Second item
        3. Third item

=== "Code"
    ````markdown
        1. First item
        5. Second item
        3. Third item
    ````


### non ordonée

=== "Rendu"
        - First item
        - Second item
        - Third item

=== "Code"
    ````markdown
        - First item
        - Second item
        - Third item
    ````

### Listes imbriquées
=== "Rendu"
        - First item
          1. Second item
          2. Third item
            - item final

=== "Code"
    ````markdown
        - First item
          1. Second item
          2. Third item
             - item final

    ````

###  Listes à cocher
=== "Rendu"       
    - [ ] A
    - [x] B
    - [ ] C

=== "Code"
    ````markdown  
        - [ ] A
        - [x] B
        - [ ] C
    ````





## Les hyperliens

Le fonctionnement des hyperliens est particulier à MkDocs, donc ce qui suit n'est pas toujours valable dans n'importe quel environnement lisant du MarkDown.

**Principe de base**
=== "Rendu"
    [Texte visible](https://www.example.com)
        
=== "Code"
    ```Markdown
    [Texte visible](https://www.example.com)
    ```

**Pour forcer l'ouverture d'un nouvel onglet du navigateur**
=== "Rendu"
    [Texte visible](https://www.example.com){:target="_blank"}
        
=== "Code"
    ```Markdown
    [Texte visible](https://www.example.com){:target="_blank"}
    ```

Il est possible de faire une référence à une partie spécifique d'un document par l'intermédiaire d'ID générées automatiquement par MkDocs pour chaque titre.

*(⚠️ les noms sont en minuscules et les caractères spéciaux - y compris les espaces, sont remplacé par des tirets. lers tirets doubles sont alors réduits à un simple tiret).*



**À l'interieur d'une page**
=== "Rendu"
    Ainsi le lien [ceci](#les-titres) renvoie ici renvoie vers le titre "Mise en forme et MarkDown".
        
=== "Code"
    ```Markdown
    Ainsi le lien [ceci](#les-titres) renvoie ici renvoie vers le titre "Mise en forme et MarkDown".
    ```

Il est possible de référer à des documents internes en utilisant leur chemin relatif.

**Vers des documents internes**
=== "Rendu"
    [Docker](../../0_Outils/20-Docker.md)
    
    [Math](./30-Math.md)

=== "Code"
    ```Markdown
    [Docker](../../0_Outils/20-Docker.md)
    
    [Math](./30-Math.md)
    ```

**Vers une partie d'un documents internes**

=== "Rendu"
    [Ressources dans la page Docker](../../0_Outils/20-Docker.md#4-ressources)

    [Ressources dans la page Math](./30-Math.md#3-ressources)
        
=== "Code"
    ```Markdown
    [Ressources dans la page Docker](../../0_Outils/20-Docker.md#4-ressources)

    [Ressources dans la page Math](./30-Math.md#3-ressources)
    ```







## Tableau
=== "Rendu"
    |cellule 1|cellule 2|
    |-------- |--------|
    |    A    |    B    |
    |    C    |    D    |


=== "Code"
    ````markdown
        |cellule 1|cellule 2|
        |-------- |--------|
        |    A    |    B    |
        |    C    |    D    |
    ````




### Notes de bas de pages

[^2]:
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

On peut écrire des maths



## Représentation de touche du clavier (Keys)
=== "Rendu"
    ++tab++ ou  ++ctrl+"R"++

=== "Code"
    ````markdown
    ++tab++ ou  ++ctrl+"R"++
    ````




----- À faire
## Quelques caractére spéciaux
=== "Rendu"
    &alpha;  &amp;

=== "Code"
    ````markdown
    &alpha;  &amp;
    ````






## Échappement des caractères

Les caractères spéciaux ayant un sens en HTML et en markdown doivent être échappés. Pour les esperluètes &amp;, chevrons < et autres caractères HTML, markdown se charge de les convertir en entités HTML lors de l’export. 

Mais si vous souhaitez utiliser dans votre texte des astérisques, accolades, dièses… à une position indiquant à markdown que vous désirer un formatage particulier, vous devez les échapper en les faisant précéder d’un antislash \. 

Sinon markdown les masquera et appliquera le formatage correspondant.


=== "Rendu"
    \* \` \- \_ \[\] \(\) \{\} \# \+ \. \! \\ 

=== "Code"
    ````markdown
    \* \` \- \_ \[\] \(\) \{\} \# \+ \. \! \\ 
    ````







## Ressources
??? note "Ressources"
    [https://www.zonensi.fr/Miscellanees/mkdocs_cmd/](https://www.zonensi.fr/Miscellanees/mkdocs_cmd/)

    [https://ericecmorlaix.github.io/adn-Tutoriel_site_web/MarkDown-Mkdocs_Material/](https://ericecmorlaix.github.io/adn-Tutoriel_site_web/MarkDown-Mkdocs_Material/)



    [https://okonore.github.io/languages_programmation/](https://okonore.github.io/languages_programmation/)



    Pour de plus amples informations, on pourra se référer à la page dédiée : <https://ens-fr.gitlab.io/mkdocs/maths/>





    [https://arminreiter.com/wp-content/uploads/2020/04/Markdown-Cheatsheet.pdf](https://arminreiter.com/wp-content/uploads/2020/04/Markdown-Cheatsheet.pdf)
    [https://www.collectiveray.com/fr/aide-m%C3%A9moire-de-d%C3%A9marque](https://www.collectiveray.com/fr/aide-m%C3%A9moire-de-d%C3%A9marque)
    [https://cheatography.com/lucbpz/cheat-sheets/the-ultimate-markdown/](https://cheatography.com/lucbpz/cheat-sheets/the-ultimate-markdown/)
    [https://enterprise.github.com/downloads/en/markdown-cheatsheet.pdf](https://enterprise.github.com/downloads/en/markdown-cheatsheet.pdf)
    [https://cheatography.com/lucbpz/cheat-sheets/the-ultimate-markdown/](https://cheatography.com/lucbpz/cheat-sheets/the-ultimate-markdown/)



    [https://blog.wax-o.com/2014/04/tutoriel-un-guide-pour-bien-commencer-avec-markdown/](https://blog.wax-o.com/2014/04/tutoriel-un-guide-pour-bien-commencer-avec-markdown/)
    [https://www.markdownguide.org/cheat-sheet/](https://www.markdownguide.org/cheat-sheet/)
    [https://www.markdownguide.org/basic-syntax](https://www.markdownguide.org/basic-syntax)
    [https://paperhive.org/help/markdown#paragraph](https://paperhive.org/help/markdown#paragraph)
    [https://markdown-guide.readthedocs.io/en/latest/basics.html](https://markdown-guide.readthedocs.io/en/latest/basics.html)



    - [https://ens-fr.gitlab.io/experience/aide-m%C3%A9moire/#les-boutons](https://ens-fr.gitlab.io/experience/aide-m%C3%A9moire/#les-boutons)
    - [https://gitlab.com/ens-fr/experience/](https://gitlab.com/ens-fr/experience/)





--------

--- À traiter
## Les bases de Markdown

!!! tip "Quelques exemples"
    |Objectif|Markdown|Rendu|
    |:-----|:------|:----|
    |Créer un lien|`[texte cliquable](https://mon_lien.fr)`|[texte cliquable](https://mon_lien.fr)|
    |Emphase faible|`Un _mot_ discret` | Un _mot_ discret |
    |Emphase forte|`Un **mot** visible` | Un **mot** visible |
    |Du code en ligne|``Une boucle `for` `` |Une boucle `for`|

### Architecture

!!! tip "La première étape"
    Un document doit être structuré en parties et sous parties.

    Le titre d'une partie (ou en-têtes) est précédé d'un ou plusieurs croisillons : `#`

    Sur un clavier azerty, Le croisillon s'obtient avec ++altgr+3++.

    On ajoute une espace après le dernier croisillon.

    On saute une ligne avant et après l'en-tête.

    === "Correct"
        !!! note "Entrée"

            ```markdown
            # Mon super titre

            Mon premier paragraphe.

            ```
        !!! done "Rendu"
            <big><strong>Mon super titre</strong></big>

            Mon premier paragraphe.

    === "Incorrect"
        !!! note "Entrée"

            ```markdown
            #Mon super titre
            Mon premier paragraphe.
            ```

        !!! fail "Rendu"
            \#Mon super titre Mon premier paragraphe.

        :warning: Ce rendu dépend du moteur...

    ??? example "Example concret"

        Pour cette page, le brouillon de structure a commencé ainsi

        ```markdown
        # Les bases du Markdown

        ## Vocabulaire et objectifs

        ## Construire une note

        ### Architecture

        Avec croisillons.

        ### Paragraphes

        Avec saut de ligne.

        ### Listes

        Avec `-`, ou `1.`

        Imbriquées...

        ### Liens

        url et image

        ### Emphase

        `_` et `*`

        ### Citations

        `>`

        ### Bloc de code

        Par décalage.

        ### Code en ligne

        Avec `` ` ``

        ### Ligne horizontale

        Avec `---`
        ```

### Paragraphes

!!! tip "Le saut de ligne"
    Pour séparer les paragraphes, il suffit de sauter une ligne.

    Il ne faut pas indenter son paragraphe dans le fichier source.

    === "Correct"
        !!! note "Entrée"

            ```markdown
            Lorem ipsum dolor sit amet, consectetur
             adipiscing elit. Proin at cursus nibh,
             et lobortis mauris. Sed tempus turpis
             quis turpis pulvinar, ac vehicula dui
             convallis. Phasellus tempus massa quam,
             ac mollis libero cursus eget.
            Donec convallis a nisl vitae scelerisque.
            Ut vel nisl id augue ullamcorper lobortis
             at id dolor.
            ```

        !!! done "Rendu"
            Lorem ipsum dolor sit amet, consectetur
             adipiscing elit. Proin at cursus nibh,
             et lobortis mauris. Sed tempus turpis
             quis turpis pulvinar, ac vehicula dui
             convallis. Phasellus tempus massa quam,
             ac mollis libero cursus eget.
            Donec convallis a nisl vitae scelerisque.
            Ut vel nisl id augue ullamcorper lobortis
             at id dolor.

    === "Incorrect"
        !!! note "Entrée"

            ```markdown
                Lorem ipsum dolor sit amet, consectetur
            adipiscing elit. Proin at cursus nibh,
            et lobortis mauris. Sed tempus turpis
            quis turpis pulvinar, ac vehicula dui
            convallis. Phasellus tempus massa quam,
            ac mollis libero cursus eget.
                Donec convallis a nisl vitae scelerisque.
                Ut vel nisl id augue ullamcorper lobortis
            at id dolor.
            ```
        
        !!! fail "Rendu"

                Lorem ipsum dolor sit amet, consectetur
            adipiscing elit. Proin at cursus nibh,
            et lobortis mauris. Sed tempus turpis
            quis turpis pulvinar, ac vehicula dui
            convallis. Phasellus tempus massa quam,
            ac mollis libero cursus eget.
                Donec convallis a nisl vitae scelerisque.
                Ut vel nisl id augue ullamcorper lobortis
            at id dolor.

        :warning: Une partie du texte devient du code en bloc, avec une police à chasse fixe.

### Aller à la ligne

!!! tip "Sans changer de paragraphe"
    Dans de **rares** cas, si on souhaite sauter de ligne, sans changer de paragraphe, on ajoute deux espaces à la fin d'une ligne.

    === "Correct"
        !!! note "Entrée"

            ```markdown
            ligne1␣␣  
            ligne2
            ```

        !!! done "Rendu"
            ligne1  
            ligne2
        
        🤞 : On a symbolisé les deux espaces avec `␣␣`, mais **il faut utiliser l'espace classique**.
        
        Ici, `␣␣` ne sert que d'illustration.

    === "Incorrect"
        !!! note "Entrée"

            ```markdown
            ligne1
            ligne2
            ```

        !!! fail "Rendu"
            ligne1
            ligne2

        :warning: Échec uniquement si on voulait forcer un saut de ligne.
    
    !!! info "Deux espaces ou plus"
        En sélectionnant le texte, vous verrez deux **véritables** espaces à la fin de `ligne 1` dans l'entrée correcte.

        Sans les deux espaces, le saut de ligne est considéré comme une unique espace entre les deux lignes, donc une seule ligne pour le navigateur.

        On peut mettre plus de deux espaces.

    !!! tip "Bonne pratique"
        Il est pratique d'avoir son code sans longue ligne,
        on peut donc les couper **dans le source où on veut**.
        Elles sont donc recollées ensuite, dans un seul paragraphe.
        Ce paragraphe sera adapté à chaque écran, c'est au navigateur de choisir les sauts de ligne.
        C'est une bonne pratique.

        On n'utilise que **rarement** des sauts de ligne forcés (avec les deux espaces en fin de ligne), sans changer de paragraphes, vous ne devriez pas en faire, sauf raison motivée. On laisse au navigateur le soin de couper les lignes en fonction de l'affichage disponible : écran large, moyen ou restreint sur téléphone.

!!! warning "Éditeur"
    - Certains éditeurs de texte suppriment les espaces de fin de ligne ; votre document serait modifié !
    - **Conseil** : il est possible, avec son éditeur, d'afficher discrètement les espaces ; c'est une bonne pratique.

### Ligne horizontale

!!! tip "Séparer deux paragraphes"
    Si on souhaite une **ligne horizontale**, on utilise `---`
     entre les deux paragraphes, avec des lignes vides de part et d'autre.

    === "Correct"
        !!! note "Entrée"

            ```markdown
            Paragraphe 1...

            ---

            Paragraphe 2...
            ```

        !!! done "Rendu"
            Paragraphe 1...

            ---

            Paragraphe 2...

    === "Incorrect"
        !!! note "Entrée"

            ```markdown
            Paragraphe 1...
            ---
            Paragraphe 2...
            ```

        !!! fail "Rendu"
            <big>**Paragraphe 1...**</big>

            Paragraphe 2...
        
        :warning: Il s'agit en fait d'une **méthode non recommandée** pour créer des en-têtes !

### Listes

!!! tip "Une ligne vide puis..."
    Une liste, comme un nouveau paragraphe, **doit être précédée d'une ligne vide**.
    Certains moteurs Markdown acceptent qu'il n'y ait pas de saut de ligne, comme avec Jupyter,
     mais pas tous.

    !!! info "Les règles"
        - Sauter une ligne avant la première puce.
        - Ensuite, on place chaque item sur une ligne en commençant par `-`
        - On peut faire des listes numérotées en commençant par `1.`
        - On place aussi une espace juste avant son item.

    !!! abstract "Liste à puce"
        !!! note "Entrée"

            ```markdown
            Markdown est **très** utilisé dans de _nombreuses_ situations

            - Jupyter
            - MkDocs
            - Discourse
            - GitLab
            - Reddit
            - Qt
            - Stack Overflow
            - Stack Exchange
            - ...
            ```

        !!! done "Rendu"
            Markdown est **très** utilisé dans de _nombreuses_ situations

            - Jupyter
            - MkDocs
            - Reddit
            - Qt
            - Stack Overflow
            - Stack Exchange
            - ...

        Pour information, la traduction pour le navigateur.
        ??? info "Code HTML"

            ```html
            <p>Markdown est <strong>très</strong> utilisé dans de <em>nombreuses</em> situations</p>
            <ul>
            <li>Jupyter</li>
            <li>MkDocs</li>
            <li>Discourse</li>
            <li>GitLab</li>
            <li>Reddit</li>
            <li>Qt</li>
            <li>Stack Overflow</li>
            <li>Stack Exchange</li>
            <li>...</li>
            </ul>
            ```

        On remarquera un peu de mise de style (l'emphase) ; nous y reviendrons.

        On remarque aussi une meilleure lisibilité du Markdown sur le HTML.

    !!! example "Liste numérotée"
        !!! note "Entrée"

            ```markdown
            Pour une liste numérotée, la numérotation est automatique !
            
            1. Mon premier exemple
            3. Le troisième, mais le deuxième a été effacé
            3. Un oubli entre le troisième et quatrième
            3. Encore un oubli...
            4. C'était le quatrième au départ.
            1. Juste un de plus qui sera numéroté correctement.
            ```
        
        !!! done "Rendu"
            Pour une liste numérotée, la numérotation est automatique !
            
            1. Mon premier exemple
            3. Le troisième, mais le deuxième a été effacé
            3. Un oubli entre le troisième et quatrième
            3. Encore un oubli...
            4. C'était le quatrième au départ.
            1. Juste un de plus qui sera numéroté correctement.

        Pour information, la traduction pour le navigateur.
        ??? info "Code HTML"

            ```html
            <p>Pour une liste numérotée, la numérotation est automatique !</p>
            <ol>
            <li>Mon premier exemple</li>
            <li>Le troisième, mais le deuxième a été effacé</li>
            <li>Un oubli entre le troisième et quatrième</li>
            <li>Encore un oubli...</li>
            <li>C&#39;était le quatrième au départ.</li>
            <li>Juste un de plus qui sera numéroté correctement.</li>
            </ol>
            ```
        
        On remarque que l'on n'a pas à se soucier de la cohérence de la numérotation dans le source, on peut donc ajouter, ou enlever des items sans revoir la numérotation.

!!! warning "Technique avancée"
    On peut imbriquer les listes numérotées ou non.

    On indente la liste imbriquée qu'il est inutile de séparer avec des lignes vides. Contrairement à la première liste.

    !!! note "Entrée"

        ```markdown
        On va faire une liste

        1. un
        1. deux
            - machin
            - chose
                31. Pour commencer à 31
                1984. Ce ne sera pas 1984 !
        1. trois
        ```

    !!! done "Rendu"
        On va faire une liste
        
        1. un
        1. deux
            - machin
            - chose
                31. Pour commencer à 31
                1984. Ce ne sera pas 1984 !
        1. trois
    
    :warning: Le rendu de la numérotation des listes imbriquées dépend du moteur. Ne pas lui faire confiance.

    De même, on peut imbriquer des paragraphes, des blocs de code, en les indentant en plus également de la même manière.

!!! info "Autres puces, indentation"
    Avec certains moteurs Markdown, il est possible d'utiliser les puces `*`, `-`, `+`.

    Nous ne recommandons d'**utiliser que les puces `-`**.

    Pour les listes imbriquées, nous recommandons une **indentation de 4 espaces**.

### Liens

!!! tip "Créer un lien avec une URL"
    Une [URL](https://fr.wikipedia.org/wiki/Uniform_Resource_Locator) permet d'identifier une ressource et le protocole associé.

    La syntaxe est `[<objet>](<url>)`
    
    - `<objet>` pourra être du texte ou une image, ou autre.
    - `<url>` pourra être une adresse web, ou mail, ou autre protocole.
    - Une adresse web peut pointer vers un document HTML, une image, un son...

    === "Adresse web"
        !!! note "Entrée"

            ```markdown
            Voici [un lien](https://startpage.com/) vers un moteur de recherche.
            ```
        
        !!! done "Rendu"
            Voici [un lien](https://startpage.com/) vers un moteur de recherche.
    
    === "Adresse mail"
        !!! note "Entrée"

            ```markdown
            Pour [écrire](mailto:john.doe@mail.com) à quelqu'un.
            ```
        
        !!! done "Rendu"
            Pour [écrire](mailto:john.doe@mail.com) à quelqu'un.
    
    === "Numéro de téléphone"
        !!! note "Entrée"

            ```markdown
            Pour [contacter rapidement](tel:+33 1 234 567 890) ce numéro.
            ```
        
        !!! done "Rendu"
            Pour [contacter rapidement](tel:+33 1 234 567 890) ce numéro.
        
        Le lien ne fonctionnera que si la page est lue avec un navigateur qui a accès au protocole. Concrètement, sur un smartphone !

    !!! warning "Technique avancée"
        On peut ajouter une info bulle au lien.

        La syntaxe est `[<objet>](<url> "<message>")`

        !!! note "Entrée"

            ```markdown
            Un autre moteur de recherche est [DuckDuckGo](https://duckduckgo.com/ "Votre confidentialité, simplifiée").
            ```
        
        !!! done "Rendu"
            Un autre moteur de recherche est [DuckDuckGo](https://duckduckgo.com/ "Votre confidentialité, simplifiée").

    !!! done "Technique simplifiée"
        Si on souhaite simplement activer l'url, sans changer le nom, ni mettre d'info-bulle.

        La syntaxe est `< url >`

        Le protocole est automatiquement déduit.

        !!! note "Entrée"

            ```markdown
            - Mon site web <https://mon_joli_site.fr>
            - Mon mail <prénom.nom@chez.moi>
            ```

        !!! done "Rendu"
            - Mon site web <https://mon_joli_site.fr>
            - Mon mail <prénom.nom@chez.moi>

### Emphase

!!! info "Pas trop gras svp !"
    Commençons par indiquer que certains documents abusent des effets de style et la lisibilité en est réduite.

    L'emphase sert à faire ressortir une partie plus importante que le reste. Ce ne peut être qu'_une_ petite partie.

!!! abstract "Définitions"
    - L'emphase faible sert à faire ressortir discrètement du texte. Elle est **souvent** en _italique_, mais on peut changer cela dans une feuille de style en cascade (CSS).
    - De même, l'emphase forte est souvent en gras, mais on pourrait décider dans la feuille CSS de modifier ce style en couleur rouge et souligné en noir. Et puis changer un autre jour ; l'ensemble du document soumis à la feuille CSS est modifié.

    ??? info "Balises HTML"
        - `<em>` pour l'emphase faible
        - `<strong>` pour l'emphase forte
        - il en existe d'autres.
        - En Markdown, on peut aussi inclure quelques balises HTML au milieu du Markdown. Ce sera dans la partie avancée... Dans un premier temps, on recommande de ne pas en inclure.

!!! tip "Astérisque et tiret-bas"
    === "L'emphase faible"
        Le tiret-bas est recommandé.

        !!! note "Entrée"

            ```markdown
            Du _texte en italique_ entouré de la balise tiret bas *ou* une étoile.
            ```

        !!! done "Rendu"
            Du _texte en italique_ entouré de la balise tiret bas *ou* une étoile.

        ??? info "HTML"

            ```html
            <p>Du <em>texte en italique</em> entouré de la balise tiret bas <em>ou</em> une étoile.</p>
            ```

    === "L'emphase forte"
        L'astérisque est recommandé.

        !!! note "Entrée"

            ```markdown
            Du **texte en gras** entouré de la balise double étoile, __ou__ double tiret bas.
            ```

        !!! done "Rendu"
            Du **texte en gras** entouré de la balise double étoile, __ou__ double tiret bas.


        ??? info "HTML"

            ```html
            <p>Du <strong>texte en gras</strong> entouré de la balise double étoile, <strong>ou</strong> double tiret bas.</p>
            ```

    === "Des mélanges"
        !!! note "Entrée"

            ```markdown
            **_Un_** exemple _avec_ tous *les* cas *__possibles__*, **fort1** ou __fort2__.
            ```

        !!! done "Rendu"
            **_Un_** exemple _avec_ tous *les* cas *__possibles__*, **fort1** ou __fort2__.


        ??? info "HTML"

            ```html
            <p><strong><em>Un</em></strong> exemple <em>avec</em> tous <em>les</em> cas <em><strong>possibles</strong></em>, <strong>fort1</strong> ou <strong>fort2</strong>.</p>
            ```

!!! warning "Recommandation"
    !!! note "Entrée"

        ```markdown
        - Pour l'emphase faible, _un_ tiret-bas.
        - Pour l'emphase forte, **deux** astérisques.
        ```

    !!! done "Rendu"
        - Pour l'emphase faible, _un_ tiret-bas.
        - Pour l'emphase forte, **deux** astérisques.


### Tableaux

!!! tip "Avec le tube"
    Le caractère `|` ([barre verticale](https://fr.wikipedia.org/wiki/Barre_verticale) ou tube) s'obtient avec ++altgr+6++.

    On construit un tableau suivant le modèle suivant.

    !!! note "Entrée"

        ```markdown
        | Objectif | Markdown | Rendu |
        |---------|-------------|---|
        | Créer un lien    | `[texte cliquable](https://mon_lien.fr)` | [texte cliquable](https://mon_lien.fr) |
        | Emphase faible   | `Un _mot_ discret`    | Un _mot_ discret   |
        | Emphase forte    | `Un **mot** visible`  | Un **mot** visible |
        | Du code en ligne | ``Une boucle `for` `` | Une boucle `for`   |
        ```
    
    !!! done "Rendu"
        | Objectif | Markdown | Rendu |
        |----------|----------|-------|
        | Créer un lien    | `[texte cliquable](https://mon_lien.fr)` | [texte cliquable](https://mon_lien.fr) |
        | Emphase faible   | `Un _mot_ discret`    | Un _mot_ discret   |
        | Emphase forte    | `Un **mot** visible`  | Un **mot** visible |
        | Du code en ligne | ``Une boucle `for` `` | Une boucle `for`   |

    ??? bug "HTML"
        Un code équivalent est bien plus délicat à écrire à la main, à lire et à maintenir...

        ```html
        <table>
        <thead>
        <tr>
        <th>Objectif</th>
        <th>Markdown</th>
        <th>Rendu</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        <td>Créer un lien</td>
        <td><code>[texte cliquable](https://mon_lien.fr)</code></td>
        <td><a href="https://mon_lien.fr">texte cliquable</a></td>
        </tr>
        <tr>
        <td>Emphase faible</td>
        <td><code>Un _mot_ discret</code></td>
        <td>Un <em>mot</em> discret</td>
        </tr>
        <tr>
        <td>Emphase forte</td>
        <td><code>Un **mot** visible</code></td>
        <td>Un <strong>mot</strong> visible</td>
        </tr>
        <tr>
        <td>Du code en ligne</td>
        <td><code>Une boucle `for` </code></td>
        <td>Une boucle <code>for</code></td>
        </tr>
        </tbody>
        </table>
        ```

    !!! info "Quelques informations"
        - Il n'est **pas nécessaire** que les tubes soient bien alignés.
        - On peut inclure du Markdown dans les cellules.
        - La seconde ligne propose des options pour chaque colonne.
            - `|:---|` pour un alignement de la colonne à gauche
            - `|---:|` pour un alignement de la colonne à droite
            - `|:--:|` pour une colonne centrée

## Exercices simples

### Facile 1

...

### Autre

...

### (NSI) Tableau donné par Python

!!! tip "Liste Python vers Markdown"
    Donner une fonction Python qui renvoie un texte en Markdown : le tableau de valeur pour une suite donnée en paramètre avec une liste.

    ```python
    def tableau_markdown(liste: list) -> str:
        lignes = []
        ajout = lignes.append
        # ... à compléter
        return "\n".join(lignes)
    ```

!!! example "Exemple"
    Pour la liste $[0, 1, 1, 2, 3, 5, 8, 13]$, on souhaite un premier rendu

    |n|0|1|2|3|4|5|6|7|
    |-|-|-|-|-|-|-|-|-|
    |u_n|0|1|1|2|3|5|8|13|

    _Un_ code Markdown valable est

    ```markdown
    |n|0|1|2|3|4|5|6|7|
    |-|-|-|-|-|-|-|-|-|
    |u_n|0|1|1|2|3|5|8|13|
    ```

    Ainsi on aimerait avoir

    ```python
    >>> tableau_markdown([0, 1, 1, 2, 3, 5, 8, 13])
    '|n|0|1|2|3|4|5|6|7|\n|-|-|-|-|-|-|-|-|-|\n|u_n|0|1|1|2|3|5|8|13|\n'
    ```

[Résoudre en ligne](https://console.basthon.fr/?script=eJx9UdtqAjEQfV_YfxjigwprcHe1imAfWvraH1Ap6e5YYmOyzaW1kI_pq9_hjzVpqhSEzgRCZs6cc5K0uAXLngUy97Rn-rVVH3IguLG4gLgNYXQLxupFnkEIwV8kGljCapMKbKecDefUoKzrULap1QNKKZy-oFH7TpyOFnVqaLROSyBrSehO8agXh4d5FpMQkmc9eDigbniDsXR_IYh0HdOWa2gRBEu6MC-gc9wAHk7HxkWcwCDbIo3jjxhmDOxVy7c89BqEN9fnAj6BAcrAZIxyJjI2aC0mUnoxE-1YNBbKPGPGoLbXT7YaF1D-rKqAuoBpEU2V9WYIyyWs08VD9L30Y1_6ytd-4qf-xs_8WvrRVYaie0rYhJ76uS_rUO__cVT942hSRQP1LFk4C1_EfgUmlY-gM2-nubQDcqfZuyLhR74B39mmdg){.md-button}
**Conseil** : Ouvrir un nouvel onglet.

??? faq "Indice 1"
    Pour construire la ligne 2, on pourrait faire

    ```python
    q = len(liste)
    ligne_2 = '|-'*(q+1) + '|'
    ajout(ligne_2)
    ```

??? faq "Indice 2"
    Pour construire la ligne 1, on pourrait faire

    ```python
    q = len(liste)
    ligne_1 = ['|n|'] # le début de la ligne 1
    for i in range(q):
        ligne_1.append(f'{i}|') # chaque morceau
    ajout("".join(ligne_1))
    ```

??? faq "Indice 3"
    La ligne 3 est très proche de la ligne 1.

??? faq "Indice 4"
    Vide.

??? done "Solution"

    ```python
    def tableau_markdown(liste: list) -> str:
        lignes = []
        ajout = lignes.append

        # ligne 1
        q = len(liste)
        ligne_1 = ['|n|'] # le début de la ligne 1
        for i in range(q):
            ligne_1.append(f'{i}|') # chaque morceau
        ajout("".join(ligne_1))

        # ligne 2
        ligne_2 = '|-'*(q+1) + '|'
        ajout(ligne_2)

        # ligne 3
        ligne_3 = ['|u_n|'] # le début de la ligne 3
        for i in range(q):
            ligne_3.append(f'{liste[i]}|') # chaque morceau
        ajout("".join(ligne_3))
        
        # ligne 4 vide !
        ajout('')

        return "\n".join(lignes)
    ```

## Markdown avancé

### Coloration syntaxique du code

!!! tip "Quelques exemples de programmes"
    === "Correct"
        !!! note "Entrée"
            ````markdown
            ```python
            for i in range(10):
                print("Salut à tous !")
            ```

            ```c
            #include<stdio.h>
            int main(){
                int i;
                for(i = 0; i < 10; i++){
                    printf("Salut à tous !\n");
                }
                return 0;
            }
            ```
            ````

        !!! done "Rendu"
            ```python
            for i in range(10):
                print("Salut à tous !")
            ```

            ```c
            #include<stdio.h>
            int main(){
                int i;
                for(i = 0; i < 10; i++){
                    printf("Salut à tous !\n");
                }
                return 0;
            }
            ```

    === "Incorrect"
        !!! note "Entrée"
            ````markdown
            for i in range(10):
                print("Salut à tous !")

            #include<stdio.h>
            int main(){
                int i;
                for(i = 0; i < 10; i++){
                    printf("Salut à tous !\n");
                }
                return 0;
            }
            ````

        !!! fail "Rendu"
            for i in range(10):
                print("Salut à tous !")

            \#include<stdio.h>
            int main(){
                int i;
                for(i = 0; i < 10; i++){
                    printf("Salut à tous !\n");
                }
                return 0;
            }

        :warning: On perd l'indentation et on peut avoir des effets non désirés !

!!! abstract "Méthode"
    Il suffit d'écrire
    
    - avant le bloc ```` ```<nom_du_langage> ````
    - après le bloc ```` ``` ````
    
    La [liste des langages supportés](https://support.codebasehq.com/articles/tips-tricks/syntax-highlighting-in-markdown) contient entre autres aussi :

    - `bash`, `diff` et `console`
    - `markdown`
    - `latex`, `tex` et `asy`
    - `html` et `css`
    - `ocaml`
    - `sql`
    - `yaml`

??? warning "Utilisation avancée"
    Suivant le même principe que pour le code en ligne, pour donner une coloration syntaxique à un bloc qui contient lui-même un bloc, on peut utiliser un accent grave `` ` `` supplémentaire dans les délimiteurs. Exemple :

    === "Correct"
        !!! note "Entrée"
            `````markdown
            ````markdown
            ```python
            print("Salut")
            ```

            ```c
            puts("Salut");
            ```
            ````
            `````
        !!! done "Rendu"
            ````markdown
            ```python
            print("Salut")
            ```

            ```c
            puts("Salut");
            ```
            ````
        Le code source de ce document comportait
        
        ``````markdown
        `````markdown
        ...
        `````
        ``````
        
        Ceci pour afficher l'entrée correcte, et deux blocs de 6 `` ` ``, pour afficher cette remarque.

    === "Incorrect"
        !!! note "Entrée"
            `````markdown
            ```markdown
            ```python
            print("Salut")
            ```

            ```c
            puts("Salut");
            ```
            ```
            `````
        !!! fail "Rendu"
            ```markdown
            ```python
            print("Salut")
            ```

            ```c
            puts("Salut");
            ```
            ```
        :warning: Rien ne peut être structuré...

Pour voir les options de numérotation et de marquage de lignes, avec MkDocs, on consultera cette [section](../markdown-mkdocs/#options-sur-les-blocs-de-code)

### Texte barré

!!! tip "On encadre d'un double `~`"
    === "Correct"
        !!! note "Entrée"
            ```markdown
            La Terre ~~est plate~~ bleue comme une orange.
            ```

        !!! done "Rendu"
            La Terre ~~est plate~~ bleue comme une orange.

    === "Incorrect"
        !!! note "Entrée"
            ```markdown
            La Terre ~~ est plate~~ bleue comme une orange.

            La Terre ~~est plate ~~ bleue comme une orange.
            ```

        !!! fail "Rendu"
            La Terre ~~ est plate~~ bleue comme une orange.

            La Terre ~~est plate ~~ bleue comme une orange.
        
        :warning: Le double `~` doit être collé au morceau à barrer.

!!! warning "Pas valable avec tous les Markdown !"
    On en dispose avec CodiMD, Jupyter, mais aussi MkDocs avec l'extension `tilde`, et nous utiliserons.

### Texte souligné

!!! tip "On encadre d'un double `^`"
    === "Correct"
        !!! note "Entrée"
            ```markdown
            La Terre bleue ^^comme^^ une orange.
            ```

        !!! done "Rendu"
            La Terre bleue ^^comme^^ une orange.

    === "Incorrect"
        !!! note "Entrée"
            ```markdown
            La Terre est bleue ^^ comme^^ une orange.

            La Terre est bleue ^^comme ^^ une orange.
            ```

        !!! fail "Rendu"
            La Terre est bleue ^^ comme^^ une orange.

            La Terre est bleue ^^comme ^^ une orange.
        
        :warning: Le double `^` doit être collé au morceau à barrer.

!!! warning "Pas valable avec tous les Markdown !"
    On en dispose avec CodiMD, mais aussi MkDocs avec l'extension `caret`, et nous utiliserons.

    Pour Jupyter, il faut utiliser la balise HTML `<u>`. :warning: Cependant le contenu ne pourra pas contenir de balise Markdown.

### Texte surligné

!!! tip "On encadre d'un double `=`"
    === "Correct"
        !!! note "Entrée"
            ```markdown
            La Terre bleue comme ==une orange==.
            ```

        !!! done "Rendu"
            La Terre bleue comme ==une orange==.

    === "Incorrect"
        !!! note "Entrée"
            ```markdown
            La Terre est bleue comme == une orange==.

            La Terre est bleue comme ==une orange ==.
            ```

        !!! fail "Rendu"
            La Terre est bleue comme == une orange==.

            La Terre est bleue comme ==une orange ==.
        
        :warning: Le double `=` doit être collé au morceau à barrer.

!!! warning "Pas valable avec tous les Markdown !"
    On en dispose avec CodiMD, mais aussi MkDocs.

    Pour Jupyter, il faut utiliser la balise HTML `<mark>`. :warning: Cependant le contenu ne pourra pas contenir de balise Markdown.

### Case à cocher

!!! tip "On préfixe par `- [ ]` ou `- [x]`"
    !!! note "Entrée"
        ```markdown
        Une liste de tâches
        
        - [ ] à faire
        - [x] fini
        - [ ] presque
        - [x] fait depuis longtemps
        ```

    !!! done "Rendu"
        Une liste de tâches
        
        - [ ] à faire
        - [x] fini
        - [ ] presque
        - [x] fait depuis longtemps

!!! abstract "Méthode"
    Comme pour une liste, mais on commence par
    
    `- [ ]` pour une case non cochée,

    `- [x]` pour une case cochée.

!!! info "Cliquables ?"
    Avec certains logiciels, les cases à cocher sont cliquables, ce qui permet de modifier leur état.

    - ❌ Avec Jupyter, les cases à cocher ne sont pas cliquables.
    - ✅ Avec CodiMD, elles le sont, et le fichier source est modifié à la volée.
    - ❓ Avec MkDocs, on peut choisir globalement sa préférence, dans ce document, elles sont cliquables.


## Liste de définitions

!!! tip "Pour une paire (clé, valeurs)"
    Les listes de définition sont idéales, par exemple pour une liste de paramètres à une fonction...

!!! example "Example"
    !!! note "Entrée"
        ```markdown
        `Lorem ipsum dolor sit amet`
        :   Sed sagittis eleifend rutrum. Donec vitae suscipit est. Nullam tempus
            tellus non sem sollicitudin, quis rutrum leo facilisis.

        `Cras arcu libero`
        :   Aliquam metus eros, pretium sed nulla venenatis, faucibus auctor ex. Proin
            ut eros sed sapien ullamcorper consequat. Nunc ligula ante.

            Duis mollis est eget nibh volutpat, fermentum aliquet dui mollis.
            Nam vulputate tincidunt fringilla.
            Nullam dignissim ultrices urna non auctor.
        ```

    !!! done "Rendu"
        `Lorem ipsum dolor sit amet`
        :   Sed sagittis eleifend rutrum. Donec vitae suscipit est. Nullam tempus
            tellus non sem sollicitudin, quis rutrum leo facilisis.

        `Cras arcu libero`
        :   Aliquam metus eros, pretium sed nulla venenatis, faucibus auctor ex. Proin
            ut eros sed sapien ullamcorper consequat. Nunc ligula ante.

            Duis mollis est eget nibh volutpat, fermentum aliquet dui mollis.
            Nam vulputate tincidunt fringilla.
            Nullam dignissim ultrices urna non auctor.
    
    :warning: En suivant la bonne indentation, on peut avoir plusieurs paragraphes dans une définition.

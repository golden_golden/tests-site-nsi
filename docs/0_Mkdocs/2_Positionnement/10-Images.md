# Images et figures




![Image title](https://dummyimage.com/600x400/eee/aaa){ align=left  width="300" loading=lazy }

ssssssssrntsnrnrstmlrstsrsssssssssssssssssssssssssssssssssss crtairtec tratciertcr orem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor massa, nec semper lorem quam in massa.

<figure  markdown>
  ![Image title](https://dummyimage.com/600x400/){ align=left  width="300" loading=lazy  }
  <figcaption>Image caption</figcaption>
</figure>




## Ressources
??? note "Ressources"
    [https://www.zonensi.fr/Miscellanees/mkdocs_cmd/](https://www.zonensi.fr/Miscellanees/mkdocs_cmd/)

    [https://ericecmorlaix.github.io/adn-Tutoriel_site_web/MarkDown-Mkdocs_Material/](https://ericecmorlaix.github.io/adn-Tutoriel_site_web/MarkDown-Mkdocs_Material/)



    [https://okonore.github.io/languages_programmation/](https://okonore.github.io/languages_programmation/)



    Pour de plus amples informations, on pourra se référer à la page dédiée : <https://ens-fr.gitlab.io/mkdocs/maths/>





    [https://arminreiter.com/wp-content/uploads/2020/04/Markdown-Cheatsheet.pdf](https://arminreiter.com/wp-content/uploads/2020/04/Markdown-Cheatsheet.pdf)
    [https://www.collectiveray.com/fr/aide-m%C3%A9moire-de-d%C3%A9marque](https://www.collectiveray.com/fr/aide-m%C3%A9moire-de-d%C3%A9marque)
    [https://cheatography.com/lucbpz/cheat-sheets/the-ultimate-markdown/](https://cheatography.com/lucbpz/cheat-sheets/the-ultimate-markdown/)
    [https://enterprise.github.com/downloads/en/markdown-cheatsheet.pdf](https://enterprise.github.com/downloads/en/markdown-cheatsheet.pdf)
    [https://cheatography.com/lucbpz/cheat-sheets/the-ultimate-markdown/](https://cheatography.com/lucbpz/cheat-sheets/the-ultimate-markdown/)



    [https://blog.wax-o.com/2014/04/tutoriel-un-guide-pour-bien-commencer-avec-markdown/](https://blog.wax-o.com/2014/04/tutoriel-un-guide-pour-bien-commencer-avec-markdown/)
    [https://www.markdownguide.org/cheat-sheet/](https://www.markdownguide.org/cheat-sheet/)
    [https://www.markdownguide.org/basic-syntax](https://www.markdownguide.org/basic-syntax)
    [https://paperhive.org/help/markdown#paragraph](https://paperhive.org/help/markdown#paragraph)
    [https://markdown-guide.readthedocs.io/en/latest/basics.html](https://markdown-guide.readthedocs.io/en/latest/basics.html)



    - [https://ens-fr.gitlab.io/experience/aide-m%C3%A9moire/#les-boutons](https://ens-fr.gitlab.io/experience/aide-m%C3%A9moire/#les-boutons)
    - [https://gitlab.com/ens-fr/experience/](https://gitlab.com/ens-fr/experience/)
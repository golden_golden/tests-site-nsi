# Disposition multi-colonnes

## 1. Qu'est ce que Multi-Columns pour mkdocs ?

Multi-Columns pour mkdocs est une page de style `css` complémentaire conçu par Rodrigo SCHWENCKE.
Celle-ci permet de tirer parti des fonctionnalités des **admonitions** fournies nativement par le **thème matériel** pour mkdocs, pour proposer des **multi-colonnes** dans mkdocs, de manière à ce qu'elles soient compatibles avec :

- Les Admonitions classiques
- Les tableaux Markdown
- Les diagrammes consut avec les plugins Graphviz et Mermaid
  

## 2. Exemples
!!! info "Remarques"
    - Les **bordures rouges** affichées dans les onglets `Rendu` ci-après, ont étés ajutées afin de vous donner une idée de la répartition des colonnes, elles ne sont évidemment pas affichés en temps normal.

    - Bien que dans les exemples présenté ci-dessous seul le mot `col` soit présent. Il faut noté que les mots clés `col`, `column` et `colonne` sont équivalents et peuvent être utilisés l'un pour l'autre.


### 2.1 Multi-colonnes de largeurs identiques

##### A. Sur deux colonnes `_2`

=== "Rendu"
    !!! col _2
        <div style="border: 2px solid red;">
        Column 1 of 2
        </div>
    !!! col _2
        <div style="border: 2px solid red;">
        Column 2 of 2
        </div>

=== "Code markdown"
    ```markdown linenums="0"
    !!! col _2
        Column 1 of 2
    !!! col _2
        Column 2 of 2
    ```


##### B. Sur trois colonnes `_3`
=== "Rendu"
    !!! col _3
        <div style="border: 2px solid red;">
        Column 1 of 3
        </div>
    !!! col _3
        <div style="border: 2px solid red;">
        Column 2 of 3
        </div>
    !!! col _3
        <div style="border: 2px solid red;">
        Column 3 of 3
        </div>

=== "Code markdown"
    ```markdown linenums="0"
    !!! col _3
        Column 1 of 3
    !!! col _3
        Column 2 of 3
    !!! col _3
        Column 3 of 3
    ```


##### C. Sur quatre colonnes `_4`

=== "Rendu"
    !!! col _4
        <div style="border: 2px solid red;">
        Column 1 of 4
        </div>
    !!! col _4
        <div style="border: 2px solid red;">
        Column 2 of 4
        </div>
    !!! col _4
        <div style="border: 2px solid red;">
        Column 3 of 4
        </div>
    !!! col _4
        <div style="border: 2px solid red;">
        Column 4 of 4
        </div>

=== "Code markdown"
    ```markdown linenums="0"
    !!! col _4
        Column 1 of 4
    !!! col _4
        Column 2 of 4
    !!! col _4
        Column 3 of 4
    !!! col _4
        Column 4 of 4
    ```

##### C. Sur cinq et six colonnes `_5` et `_6`

On utilise la même syntax pour cinq et six colonnes.

- Pour $5$ colonnes :  la syntax est `_5` (syntaxe identique à  `_2` ou `_3` ou `_4`)
- Pour $6$ colonnes :  la syntax est `_6`




### 2.2 Multi-colonnes de largeurs différentes 

Les colonnes peuvent également avoir des **largeurs différentes**, la syntaxe utilise alors **DEUX underscores** suivis d'une valeur entière représentant **un pourcentage de la largeur du parent**.

!!! info "Remarque"
    La somme des largeurs devra alors être égale à 100%.

    Si ce n'est pas le cas, c'est-à-dire que la  **somme des largeurs est différentes de 100 %**, le comportement par défaut est que le contenu suivant continuera à s'accumuler après la dernière colonne sous la forme `float:left` ; fait en `CSS`.
    Ceci peut être modifié avec le mot-clé `clear`.


#### A. Sur la totalité de la largeur du parent

=== "Rendu"
    !!! col __17
        <div style="border: 2px solid red;">
        17% width
        </div>
    !!! col __60
        <div style="border: 2px solid red;">
        60% width
        </div>
    !!! col __23
        <div style="border: 2px solid red;">
        23% width
        </div>

=== "Code markdown"
    ```markdown linenums="0"
    !!! col __17
        17% width
    !!! col __60
        60% width
    !!! col __23
        23% width
    ```

#### B. Seulement une partie de la largueur du parent est occupée

Le comportement par défaut est d'accumuler les contenus (colonnes ou autres contenus : textes, images, etc.) sous la forme d'un `float:left` après la dernière colonne. Ceci peut être modifié avec le mot-clé `clear`.

##### B.1 Avec un espace vide

=== "Rendu"
    !!! col __18
        <div style="border: 2px solid red;">
        18% width
        </div>
    !!! col __27
        <div style="border: 2px solid red;">
        27% width
        </div>
    !!! col __10 clear
        <div style="border: 2px solid red;">
        10% width
        </div>
    lorem ipsum bla bla bla

=== "Code markdown"
    ```markdown linenums="0"
    !!! col __18
        18%
    !!! col __27
        27%
    !!! col __10 clear
        10%
    lorem ipsum bla bla bla
    ```

##### B.2 Sans espace vide

=== "Rendu"
    !!! col __18
        <div style="border: 2px solid red;">
        18% width
        </div>
    !!! col __27
        <div style="border: 2px solid red;">
        27% width
        </div>
    !!! col __10
        <div style="border: 2px solid red;">
        10% width
        </div>
    lorem ipsum bla bla bla

=== "Code markdown"
    ```markdown linenums="0"
    !!! col __18
        18%
    !!! col __27
        27%
    !!! col __10
        10%
    lorem ipsum bla bla bla
    ```


## Colonnes alignées à droite `right`
Le comportement par défaut décrit précédemment (accumuler les colonnes à gauche) peut être modifié avec le mot-clé `right`.


##### B.1 Avec un espace vide

=== "Rendu"
    !!! col __18 right
        <div style="border: 2px solid red;">
        18% width
        </div>
    !!! col __27 right
        <div style="border: 2px solid red;">
        27% width
        </div>
    !!! col __10 right clear
        <div style="border: 2px solid red;">
        10% width
        </div>
    lorem ipsum bla bla bla

=== "Code markdown"
    ```markdown linenums="0"
    !!! col __18 right
        18%
    !!! col __27 right
        27%
    !!! col __10 right clear
        10%
    lorem ipsum bla bla bla
    ```

##### B.2 Sans espace vide

=== "Rendu"
    !!! col __18 right
        <div style="border: 2px solid red;">
        18% width
        </div>
    !!! col __27 right
        <div style="border: 2px solid red;">
        27% width
        </div>
    !!! col __10 right
        <div style="border: 2px solid red;">
        10% width
        </div>
    lorem ipsum bla bla bla

=== "Code markdown"
    ```markdown linenums="0"
    !!! col __18 right
        18%
    !!! col __27 right
        27%
    !!! col __10 right
        10%
    lorem ipsum bla bla bla
    ```

## Centrer le contenue d'une colonne avec `center`
Le centrage des textes et contenus est possible avec le mot clé `center`.

### Avec `Center`

=== "Rendu"
    !!! col _2 center
        <div style="border: 2px solid red;">
        Column 1 of 2
        </div>
    !!! col _2 center
        <div style="border: 2px solid red;">
        ![Bateau](assets/images/bateau.jpg)
        </div>

=== "Code markdown"
    ```markdown linenums="0"
    !!! col _2 center
        Column 1 of 2
    !!! col _2 center
        ![Bateau](assets/images/bateau.jpg)
    ```

##### B.2 Sans `center`

=== "Rendu"
    !!! col _2
        <div style="border: 2px solid red;">
        Column 1 of 2
        </div>
    !!! col _2
        <div style="border: 2px solid red;">
        ![Bateau](assets/images/bateau.jpg)
        </div>

=== "Code markdown"
    ```markdown linenums="0"
    !!! col _2
        Column 1 of 2
    !!! col _2
        ![Bateau](assets/images/bateau.jpg)
    ```

### `center` et `right`

`center` keyword is compatible with the `right` keyword:

=== "Rendu"
    !!! col __18 right center
        <div style="border: 2px solid red;">
        18% width
        </div>
    !!! col __27 right center
        <div style="border: 2px solid red;">
        27% width
        </div>
    !!! col __10 right center clear
        <div style="border: 2px solid red;">
        10% width
        </div>
    lorem ipsum bla bla bla

=== "Code markdown"
    ```markdown linenums="0"
    !!! col __18 right center
        18%
    !!! col __27 right center
        27%
    !!! col __10 right center clear
        10%
    lorem ipsum bla bla bla
    ```




## Compatibilité avec les Admonitions

=== "Rendu"
    !!! col __18
        !!! info
            info admonition with 18% width
    !!! col __27
        !!! tip
            tip admonition with 27% width
    !!! col __45 clear
        !!! warning
            warning admonition with 45% width
    lorem ipsum bla bla bla

=== "Code markdown"
    ```bash linenums="0"
    !!! col __18
        !!! info
            info admonition with 18% width  
    !!! col __27  
        !!! tip  
            tip admonition with 27% width  
    !!! col __45 clear  
        !!! warning  
            warning admonition with 45% width  
    lorem ipsum bla bla bla  
    ```


## 2. Installation

Si vous utilisez l'image docker associée à ce site et le dépot de basse de ce site tout est installé et configurée ! La note ci-dessous vous est donc inutile.

??? note "Installation" 
    Dans le dossier de vos styles personnel, pour moi `xtrat`, ajouter le fichier `mkdocs-columns.css` disponible : [à faire ]()

    Dans le fichier de configuration `mkdocs.yml` ajouter dans les extra-css, le lien vers le fichier `mkdocs-columns.css` :
 
    ```yaml linenums="0"
    extra_css:
        - xtra/stylesheets/mkdocs-columns.css 
  
    ```

## 3. Remarque
??? note "Remarque"
    Sur le site de Rodrigo Schwenche, concepteur du fichier css :  [https://eskool.gitlab.io/mkhack3rs/columns/](https://eskool.gitlab.io/mkhack3rs/columns/)

    Vous trouverez pour l'installations : 
    ```yaml linenums="0"
    extra_css:
        # For mkdocs integration: Add a link to 'columns.css' cdn by rod2ik
        - https://cdn.jsdelivr.net/gh/rod2ik/cdn@main/mkdocs/css/columns.css
  
    ```
    Cette solution à pour avantage de simplifier l'installation *(une seule ligne à ajouter au fichier `mkdocs.yml` et c'est tout)* et de bénéficer des mises à jours de Rodrigo Schwenche, cepandant elle à pour défaut de modifier  d'autres styles comme par exemples ceux des titres !


## 4. Ressources

??? note "Ressources"
    - Merci à Rodrigo SCHWENCKE dont cette pages est entièrement issu : [https://eskool.gitlab.io/mkhack3rs/columns/](https://eskool.gitlab.io/mkhack3rs/columns/)
# Kroki

## 1. Exemple
!!! note ""
    === "Rendu"
        ```kroki-blockdiag
        blockdiag {
        blockdiag -> generates -> "block-diagrams";
        blockdiag -> is -> "very easy!";

        blockdiag [color = "greenyellow"];
        "block-diagrams" [color = "pink"];
        "very easy!" [color = "orange"];
        }
        ```

    === "Code"
        ````markdown
            ```kroki-blockdiag
            blockdiag {
                blockdiag -> generates -> "block-diagrams";
                blockdiag -> is -> "very easy!";

                blockdiag [color = "greenyellow"];
                "block-diagrams" [color = "pink"];
                "very easy!" [color = "orange"];
            }
            ```
        ````
## 2. Installation
Si vous utilisez l'image docker associée à ce site et le dépot de basse de ce site tout est installé et configurée ! La note ci-dessous vous est donc inutile.

??? note "Installation de Kroki"
    À faire


## 3. Ressources

??? note "Ressources"
    Le site officiel : [https://kroki.io/](https://kroki.io/)





# Mermaid


## 1. Exemples
### A. Graphes et arbres



!!! note ""
    === "Rendu"
        ```mermaid
            graph TD;
                A-->B;
                A-->C;
                B-->D;
                C-->D;
        ```

    === "Code"
        ````markdown
  

        ```mermaid
        graph TD;
            A-->B;
            A-->C;
            B-->D;
            C-->D;
        ```
        ````


### B. Diagramme de classe

!!! note ""
    === "Rendu"
        ```mermaid
        classDiagram
            Class01 <|-- AveryLongClass : Cool
            Class03 *-- Class04
            Class05 o-- Class06
            Class07 .. Class08
            Class09 --> C2 : Where am i?
            Class09 --* C3
            Class09 --|> Class07
            Class07 : equals()
            Class07 : Object[] elementData
            Class01 : size()
            Class01 : int chimp
            Class01 : int gorilla
            Class08 <--> C2: Cool label
        ```

    === "Code"
        ````markdown
        ```mermaid
        classDiagram
            Class01 <|-- AveryLongClass : Cool
            Class03 *-- Class04
            Class05 o-- Class06
            Class07 .. Class08
            Class09 --> C2 : Where am i?
            Class09 --* C3
            Class09 --|> Class07
            Class07 : equals()
            Class07 : Object[] elementData
            Class01 : size()
            Class01 : int chimp
            Class01 : int gorilla
            Class08 <--> C2: Cool label
        ```
        ````

### C. Git

!!! note ""
    === "Rendu"
        ```mermaid
        gitGraph
            commit id: "C1"
            commit id: "C2"
            branch dev
            checkout dev
            commit id: "C3"
            commit id: "C4"
            checkout main
            merge dev
            commit id: "C5"
            commit id: "C6"
        ```

    === "Code"
        ````markdown
        
        ```mermaid
        gitGraph
            commit id: "C1"
            commit id: "C2"
            branch dev
            checkout dev
            commit id: "C3"
            commit id: "C4"
            checkout main
            merge dev
            commit id: "C5"
            commit id: "C6"
        ```
        ````



## 2. Installation
Si vous utilisez l'image docker associée à ce site et le dépot de basse de ce site tout est installé et configurée ! La note ci-dessous vous est donc inutile.

??? note "Installation des macro"
    À faire


## 3. Ressources
??? note "Ressources"
    - Merci à Franck CHAMBON et à sont dépot : [https://gitlab.com/ens-fr/exp2/-/tree/main/](https://gitlab.com/ens-fr/exp2/-/tree/main/)

    - Merci au forum NSI pour la découverte de cet outil : [ Mermaid, outil simple pour les diagrammes etc - Forum NSI](https://mooc-forums.inria.fr/moocnsi/t/mermaid-outil-simple-pour-les-diagrammes-etc/5082)

    - Le site officiel : [Mermaid](https://mermaid-js.github.io/mermaid/#/)

    - [https://eskool.gitlab.io/mkhack3rs/mermaid/examples/](https://eskool.gitlab.io/mkhack3rs/mermaid/examples/)



À traiter
[https://mooc-forums.inria.fr/moocnsi/t/mermaid-outil-simple-pour-les-diagrammes-etc/5082/15](https://mooc-forums.inria.fr/moocnsi/t/mermaid-outil-simple-pour-les-diagrammes-etc/5082/15)
# Docker

<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/XgKOC6X8W28" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>

## 1. Qu'est-ce que docker
## 2. Installer docker
### 2.1 Sur window
[Installer Docker sur Windows](https://practicalprogramming.fr/install-docker-windows/)
[https://korben.info/installer-docker-windows-home.html](https://korben.info/installer-docker-windows-home.html)

### 2.2 Sur Linux


## 3. Utiliser docker


## 4. Ressources

??? remarque "Ressources"
    - [Korben_initiation à docker pour tous](https://www.youtube.com/watch?v=Jpesrg2R9ag)
    - [Cookie connecté_Docker: comprendre l'essentiel en 7 minutes](https://www.youtube.com/watch?v=caXHwYC3tq8)
    - [Grafikart.fr_Tutoriel Docker : Présentation de Docker](https://www.youtube.com/watch?v=XgKOC6X8W28)
    - [cocadmin_Docker: Débuter de zéro avec Docker en français - Tutoriel 1/3](https://www.youtube.com/watch?v=SXB6KJ4u5vg)
    - [TheiPhoneRetro_Bien comprendre et maîtriser Docker !](https://www.youtube.com/watch?v=jkRGw3zgHuQ)
    - [kevin JULLIEN_Introduction à Docker - Concept et Prise en Main](https://www.youtube.com/watch?v=LR4UE6isrLU)
    - [xavki_premier pas avec Docker](https://www.youtube.com/watch?v=fdlZqRZXWOc)


# Git

Utilitaire pour la gestion de version et le travail colaboratif.

## Qu'est ce que Git
Présentation rapide 

[https://git-scm.com/downloads](https://git-scm.com/downloads)

[https://git-scm.com/book/fr/v2/](https://git-scm.com/book/fr/v2/)


# 2. Gestionnaire de version

### 2.1 Utilité d'un gestionnaire de version

[](https://www.youtube.com/watch?v=hPfgekYUKgk)

[](https://www.youtube.com/watch?v=gp_k0UVOYMw)



### 2.2 Utilisation de git
[https://www.pierre-giraud.com/git-github-apprendre-cours/](https://www.pierre-giraud.com/git-github-apprendre-cours/)
[https://git-scm.com/docs](https://git-scm.com/docs)

[y](https://www.youtube.com/watch?v=hPfgekYUKgk)

[y](https://www.youtube.com/watch?v=rP3T0Ee6pLU&list=PLjwdMgw5TTLXuY5i7RW0QqGdW0NZntqiP)



### 2.3 Utilisation de gitlab et github

gitlab et GitHub sont des dépôts distant pour utiliser git. 

[y](https://www.youtube.com/watch?v=GVEJJQUDVz4)

[y](https://www.youtube.com/watch?v=iub0_uVWGmg)

[y](https://www.youtube.com/watch?v=76hFN6kcV04)


#### Liens relatif à git (versionning):
- [y](https://www.youtube.com/watch?v=gp_k0UVOYMw)
  
- [y](https://www.youtube.com/watch?v=OBLbCWeR4ac)
  
- [y](https://www.youtube.com/watch?v=rP3T0Ee6pLU&list=PLjwdMgw5TTLXuY5i7RW0QqGdW0NZntqiP)


??? remarque "gitkraken"
    - [Grafikart.fr_comprendre Git (18/18)](https://www.youtube.com/watch?v=daBPgzan_wI)
  
    - [La teck  avec Bertrand_Git: les bases!](https://www.youtube.com/watch?v=225wkShIcc8)

    - [inria ](https://www.youtube.com/watch?v=iub0_uVWGmg)
---
   title : Windows WSL

---


# Installer le shell Bash Linux sous Windows 10 avec WSL
## Pourquoi utiliser WSL en NSI
Dans le programme de NSI, il est demmandé :


## Installation et utilisation de WSL

<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/CyG16N3GJWo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>


??? remarque "Ressources"
     - [Korben_Installer le shell Bash Linux sous Windows 10 avec WSL](https://www.youtube.com/watch?v=CyG16N3GJWo)
   
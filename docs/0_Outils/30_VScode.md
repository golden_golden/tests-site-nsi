---
   title : VSCode

---


# L'éditeur de code : VSCode ou VSCodium
VScodium est la versions libre de VSCode





## Les liens

### installation/configurations
#### FormationVidéo : https://www.youtube.com/watch?v=eQUsUq_2AQU

[dans le navigateur](https://korben.info/visual-studio-code-dans-votre-navigateur.html)



### Pour python
#### Docstring : 
- https://www.youtube.com/watch?v=gzj13PlMmfk



### Pour du Web :
#### Le Designeur du Web : https://www.youtube.com/watch?v=w65jd0Vyj_w



Autres :
- https://www.youtube.com/watch?v=yp3XKlqMXh0


## Installation/configurations
### Sur linux
### Sur Window
### Sur Mac

## Les extensions
### Pour le Web
### Pour le python


### Autres
#### Pour le Javascript/Typescript
#### Pour git
#### Pour Docker


## Les raccourci claviers utiles


??? remarque "Ressources"
     - [TheiPhoneRetro_Mes extensions favorites pour Visual Studio Code (éditeur de code) #1](https://www.youtube.com/watch?v=dc21bhbnrZk)
     - [TheiPhoneRetro_Mes extensions favorites pour Visual Studio Code (éditeur de code) #2](https://www.youtube.com/watch?v=xzt3raZ7sfU)
# Le Terminal




## 4. Ressources

??? remarque "Ressources"
   - [simulateur en ligne https://nsi42.net/](https://nsi42.net/)
   - [https://luffah.xyz/bidules/Terminus/](https://luffah.xyz/bidules/Terminus/)


   - [https://mooc-forums.inria.fr/moocnsi/t/logiciels-libres-au-lycee-retour-dexperience/6541/12](https://mooc-forums.inria.fr/moocnsi/t/logiciels-libres-au-lycee-retour-dexperience/6541/12)
   - [https://www.youtube.com/watch?v=U2b-MYcSCLc](https://www.youtube.com/watch?v=U2b-MYcSCLc)
   - [Docstring le terminal : 10 commandes indispensables_https://www.youtube.com/watch?v=NgK-FaWJyqQ](https://www.youtube.com/watch?v=NgK-FaWJyqQ)
   - [https://www.youtube.com/watch?v=4jlPZtc17l8](https://www.youtube.com/watch?v=4jlPZtc17l8)


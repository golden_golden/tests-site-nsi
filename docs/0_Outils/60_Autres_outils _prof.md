---
   title : D'autres outils prof

---


enregistrer son écran : OBS



---- À voir :
Créez des captures écrans explicatives et partagez les avec le monde entier (extension Firefox) : [https://korben.info/captures-ecrans-explicatives-et-partagez-les-avec-le-monde-entier-extension-firefox.html](https://korben.info/captures-ecrans-explicatives-et-partagez-les-avec-le-monde-entier-extension-firefox.html)


Faire des gifs :
- [https://korben.info/creer-des-gif-animes.html](https://korben.info/creer-des-gif-animes.html)
- [https://korben.info/filmez-une-partie-de-votre-ecran-et-faites-en-un-gif-anime.html](https://korben.info/filmez-une-partie-de-votre-ecran-et-faites-en-un-gif-anime.html)
- [https://korben.info/creez-et-editez-vos-gif-animes-avec-ezgif.html](https://korben.info/creez-et-editez-vos-gif-animes-avec-ezgif.html)
- [https://korben.info/gifski-encoder-creez-vos-propres-gif-animes-a-partir-de-videos.html](https://korben.info/gifski-encoder-creez-vos-propres-gif-animes-a-partir-de-videos.html)
- [https://korben.info/creer-gifs-youtube.html](https://korben.info/creer-gifs-youtube.html)
- [https://korben.info/gifcurry-lediteur-open-source-des-createurs-de-gifs.html](https://korben.info/gifcurry-lediteur-open-source-des-createurs-de-gifs.html)
- [https://korben.info/gifcam-utilitaire-faire-vos-propres-gifs-animes.html](https://korben.info/gifcam-utilitaire-faire-vos-propres-gifs-animes.html)
---
   title : D'autres outils

---

## Ent et autres services de l'état

**capytal**
- [Capytale](https://capytale2.ac-paris.fr/web/accueil?check_logged_in=1)
- [Présentation, webinaire](https://capytale2.ac-paris.fr/wiki/lib/exe/fetch.php?media=webinaire-capytale_2022-06-14.mp4)
  il y a une démarche à faire pour intégrer capytale à l'ent (*ps: possible en occitanie*)
- [capytal autocorection](https://github.com/capytale/capytale/blob/master/PRES%20nbgrader%20(tests%20unitaires%20et%20auto-correction).ipynb)
- [capytal correction rendu](https://www.lecluse.fr/info/jupyter/nbgrader/)
- [capytal docker image](https://plmlab.math.cnrs.fr/docker-images/base-notebook-nbgrader/-/tree/master/)


**Autre**

- [Peertube](https://apps.education.fr/applications/peertube/)
- [Pix](https://pix.fr/)


## Python 
### Ide
- [Installation standard avec Idle](https://www.python.org/downloads/)
[édupython (ce qui est installé sur les oridinateurs du lycée)](https://edupython.tuxfamily.org/)
- [Installation Thonny (debugger très pratique)](https://thonny.org/)
- [Installation Anaconda + Pyzo](https://pyzo.org/start.html)


### Bibliothèque Python
#### Pour du jeux vidéo
- [pyxel](https://github.com/kitao/pyxel)
[Ressources documentation](https://nuitducode.github.io/DOCUMENTATION/PYTHON/01-presentation/)
[exemple de jeu pyxel](https://mooc-forums.inria.fr/moocnsi/uploads/short-url/wjeeeEiQ5ONuqjAXMvv460DNeCB.pdf)
- [pygame]()
- [Pygame Zéro]()

???+ note "micro studio"
  - [https://mooc-forums.inria.fr/moocnsi/t/microstudio-dev-pour-les-projets-nsi/6672](https://mooc-forums.inria.fr/moocnsi/t/microstudio-dev-pour-les-projets-nsi/6672)
  - [https://microstudio.dev/fr](https://microstudio.dev/fr)


### framwork
- [Flask]()
- [Django]()

### Outils de débug
- [https://pythontutor.com/](https://pythontutor.com/)
 

## Graphe
- [https://frederic.zinelli.gitlab.io/graph-application/](https://frederic.zinelli.gitlab.io/graph-application/)
- [https://mooc-forums.inria.fr/moocnsi/t/algographe-application-sur-les-graphes-algorithmes-sur-les-graphes/6087/49](https://mooc-forums.inria.fr/moocnsi/t/algographe-application-sur-les-graphes-algorithmes-sur-les-graphes/6087/49)



## Brouiller du code python pour le distribuer aux élèves sans fournir les réponces
[https://mooc-forums.inria.fr/moocnsi/t/donner-des-programmes-sans-reveler-le-code/3892/9](https://mooc-forums.inria.fr/moocnsi/t/donner-des-programmes-sans-reveler-le-code/3892/9)

## HTML, CSS, js






## Réseau


??? remarque "Ressources"
     - [Korben_Installer le shell Bash Linux sous Windows 10 avec WSL](https://www.youtube.com/watch?v=CyG16N3GJWo)
   


## Évènements, concours
[https://mooc-forums.inria.fr/moocnsi/t/journees-nationale-nsi-et-trophees-nsi/6077](https://mooc-forums.inria.fr/moocnsi/t/journees-nationale-nsi-et-trophees-nsi/6077)

!!! note "la nuit de code"
    Bonjour,
    Effectivement, il y a la Nuit du c0de 4. Cette année c’était la 6e édition et pour la première fois, en plus de Scratch, Python a été proposé. Gros succès! Les jeux en Python se font avec [pyxel](https://github.com/kitao/pyxel) 5 (équivalent Python de Pico-8). Les limites (volontaires) du moteur de jeu font que les élèves peuvent se concentrer sur le code et le scénario.
    L’introduction de Python dans la Nuit du c0de est donc validée. La nuit du c0de se fera avec Scratch et Python pour les prochaines éditions.
    La Nuit du c0de n’est pas une compétition. Le but est que les élèves passent un bon moment à coder un jeu, s’entraider, partager entre groupes, échanger… La Nuit du c0de est organisée par des collègues dans les établissements. Tous les élèves du CM1 à la Terminale peuvent participer. Il n’y a pas véritablement de classement. Chaque établissement propose deux jeux par catégorie (trois catégories Scratch et deux catégories Python) pour le classement international (qui est en train de se faire), et à l’issue des évaluations, huit jeux par catégorie seront publiés (donc pas de première, deuxième, troisième… juste une liste de huit jeux). Cette année nous avions plus de 4500 élèves de 159 établissements différents (141 villes, 38 pays).
    Pour Python / Pyxel, des ressources ont été publiées pour l’édition de cette année : [Ressources documentation](https://nuitducode.github.io/DOCUMENTATION/PYTHON/01-presentation/).
    Ces ressources vont s’étoffer. D’ailleurs, si vous voulez contribuer, n’hésitez pas à me contacter.
    laurentabbal (forum nsi)

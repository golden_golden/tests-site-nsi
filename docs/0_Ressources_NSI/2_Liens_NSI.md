---
title: `Ressources NSI`

---

----à traiter
- [https://kxs.fr/cours/](https://kxs.fr/cours/)
- [https://nsi4noobs.fr/](https://nsi4noobs.fr/)
- [https://nsijoliotcurie.fr/TNSI/](https://nsijoliotcurie.fr/TNSI/)
- [https://gitlab.com/users/Peio47/projects](https://gitlab.com/users/Peio47/projects)
- [https://gitlab.com/Peio47/terminale/-/tree/main](https://gitlab.com/Peio47/terminale/-/tree/main)
- [https://ens-fr.gitlab.io/algo2/1-Python/6-ressources/](https://ens-fr.gitlab.io/algo2/1-Python/6-ressources/)
- [https://eskool.gitlab.io/tnsi/ressources/tnsi_sites_enseignants/](https://eskool.gitlab.io/tnsi/ressources/
tnsi_sites_enseignants/)
- [https://github.com/ericECmorlaix/TNSI_2021-2022](https://github.com/ericECmorlaix/TNSI_2021-2022)


- [https://ens-fr.gitlab.io/algo2/](https://ens-fr.gitlab.io/algo2/)



------- En cours
???+ note "
Informatique au lycée Prévert  *Auteur : Angélique BEUCHER *"
    === "Mkdocs"
        [https://infosite27.forge.aeif.fr/informatique-au-lycee-prevert/TalNSI/talnsi/](https://infosite27.forge.aeif.fr/informatique-au-lycee-prevert/TalNSI/talnsi/)

    === "Dépot"
        [https://forge.aeif.fr/infosite27/informatique-au-lycee-prevert](https://forge.aeif.fr/infosite27/informatique-au-lycee-prevert)

    === "Citer"
        Copyleft Angélique BEUCHER CC-BY-NC 






???+ note "A Propos-terminale *Auteur : Fabrice Nativel*"
    === "Mkdocs"
        [https://fabricenativel.github.io/NSITerminale/](https://fabricenativel.github.io/NSITerminale/)

    === "Dépot"
        [https://github.com/fabricenativel/NSITerminale](https://github.com/fabricenativel/NSITerminale)

    === "Citer"
        Fabrice Nativel CC BY 4.0

        Site correspondant au cours de NSI en classe terminale du lycée Mémona Hintermann-Afféjee.


???+ note "A Propos-première *Auteur : Fabrice Nativel*"
    === "Mkdocs"
        [https://fabricenativel.github.io/NSIPremiere/](https://fabricenativel.github.io/NSIPremiere/)

    === "Dépot"
        [https://github.com/fabricenativel/NSIPremiere](https://github.com/fabricenativel/NSIPremiere)

    === "Citer"
        Fabrice Nativel CC BY 4.0

        Site correspondant au cours de NSI en classe terminale du lycée Mémona Hintermann-Afféjee.



???+ note "Cours de terminale *Auteur :Vincent-Xavier Jumel *"
    === "Site"
        [https://lamadone.frama.io/informatique/terminale-nsi/index.html](https://lamadone.frama.io/informatique/terminale-nsi/index.html)

    === "Dépot"
        [https://framagit.org/lamadone/informatique/terminale-nsi](https://framagit.org/lamadone/informatique/terminale-nsi)

    === "Citer"
        Vincent-Xavier Jumel
        
        licence CC-by-sa 4.0


???+ note "Cours de Premiere *Auteur :Vincent-Xavier Jumel *"
    === "Site"
        [https://lamadone.frama.io/informatique/premiere-nsi/](https://lamadone.frama.io/informatique/premiere-nsi/)

    === "Dépot"
        [https://framagit.org/lamadone/informatique/premiere-nsi](https://framagit.org/lamadone/informatique/premiere-nsi)

    === "Citer"
        Vincent-Xavier Jumel
        
        licence CC-by-sa 4.0




???+ note "NSI_T *Auteur : Olivier Lecluse*"
    === "Site"
        [https://www.lecluse.fr/nsi/NSI_T/](https://www.lecluse.fr/nsi/NSI_T/)

    === "Dépot"
        [https://framagit.org/lecluseo](https://framagit.org/lecluseo)

    === "Citer"
        Olivier Lecluse 

        Licences CC BY-SA 4.0


???+ note "SNT *Auteur : Olivier Lecluse*"
    === "Site"
        [https://www.lecluse.fr/snt/](https://www.lecluse.fr/snt/)

    === "Dépot"
        [https://framagit.org/lecluseo](https://framagit.org/lecluseo)

    === "Citer"
        Olivier Lecluse 

        Licences CC BY-SA 4.0






???+ note "Python des Neiges *Auteur : Sébastien HOARAU*"
    === "Mkdocs"
        [https://sebhoa.gitlab.io/iremi/](https://sebhoa.gitlab.io/iremi/)

    === "Dépot"
        [https://gitlab.com/sebhoa/iremi](https://gitlab.com/sebhoa/iremi)

    === "Citer"
        Sébastien HOARAU

        CC BY-NC-SA 4.0



???+ note " *Auteur : Yves Cadour*"
    === "Mkdocs"
        []()

    === "Dépot"
        [https://github.com/saintlouis29/coursNSI/tree/3116e400f931c33c295b11a381e8fd553d0a04c5](https://github.com/saintlouis29/coursNSI/tree/3116e400f931c33c295b11a381e8fd553d0a04c5)

    === "Citer"
        Yves Cadour

        Le lycée Saint Louis de Chateaulin

        licences : ???



------- En cours à faire















???+ note " *Auteur : *"
    === "Mkdocs"
        []()

    === "Dépot"
        []()

    === "Citer"


???+ note " *Auteur : *"
    === "Mkdocs"
        []()

    === "Dépot"
        []()

    === "Citer"


???+ note " *Auteur : *"
    === "Mkdocs"
        []()

    === "Dépot"
        []()

    === "Citer"


???+ note "MARC SILANUS *Auteur : Marc Silanus*"
    === "Mkdocs"
        [https://silanus.fr/](https://silanus.fr/)

    === "Dépot"
        inconnu

    === "Citer"
        Marc Silanus 
        licences inconnu 


------

------ En cours
???+ note " *Auteur : Angellier *"
    === "Mkdocs"
        [https://angellier.gitlab.io/nsi/terminale/](https://angellier.gitlab.io/nsi/terminale/)

    === "Dépot"
        []()

    === "Citer"


???+ note " *Auteur : *"
    === "Mkdocs"
        [https://qkzk.xyz/docs/nsi/cours_terminale/](https://qkzk.xyz/docs/nsi/cours_terminale/)

    === "Dépot"
        []()

    === "Citer"




???+ note " NSI *Auteur : *"
    === "Mkdocs"
        [https://www.numerique-sciences-informatiques.fr/coursNsiTerminaleMenu.php](https://www.numerique-sciences-informatiques.fr/coursNsiTerminaleMenu.php)

    === "Dépot"
        []()

    === "Citer"


???+ note "SNT *Auteur : *"
    === "Mkdocs"
        []()

    === "Dépot"
        [https://www.numerique-sciences-informatiques.fr/index.php](https://www.numerique-sciences-informatiques.fr/index.php)

    === "Citer"    



???+ note "Cours de première et terminale*Auteur : Alain Busser*"
    === "Site"
        [https://alainbusser.frama.io/NSI-IREMI-974/](https://alainbusser.frama.io/NSI-IREMI-974/)

    === "Dépot"
        Inconnu

    === "Citer"
        Alain Busser

        CC BY-SA 4.0


----








# Ressources de cours de NSI

## 1. Cours de Première
### 1.1 Mkdocs et dépot git de collègue

??? note "Pixees *Auteur : David Roche*"
    === "Mkdocs"
        [https://dav74.github.io/site_nsi_prem/](https://dav74.github.io/site_nsi_prem/)

    === "Dépot"
        [https://github.com/dav74/site_nsi_prem](https://github.com/dav74/site_nsi_prem)

    === "Citer"
        David Roche, enseignant au lycée G Fichet de Bonneville (74)

        [https://github.com/dav74/site_nsi_term/blob/main/LICENSE](https://github.com/dav74/site_nsi_term/blob/main/LICENSE)
        
        CC0 1.0 Universal


??? note "eskool *Auteur : Rodrigo SCHWENCKE, colaboratif liste nsi*"
    === "Mkdocs"
        [https://eskool.gitlab.io/1nsi/](https://eskool.gitlab.io/1nsi/)

    === "Dépot"
        [https://gitlab.com/eskool/1nsi](https://gitlab.com/eskool/1nsi)

    === "Citer"
        En acceptant de publier des contenus pour votre discipline, sous Licence CC-BY-NC-SA (CC - Creative Commons, BY- Citation de l'auteur, NC - Non Commercial, SA - Share Alike - Partage dans les Mêmes Conditions)?

        Contactez-nous. Nous serons heureux de travailler avec vous, et tâcherons de développer des fonctionnalités adaptées à votre discipline.
        

  
??? note "Première NSI Lycée du Parc *Auteur : Frederic-junier*"
    === "Mkdocs"
        [https://frederic-junier.org/NSI/premiere/](https://frederic-junier.org/NSI/premiere/)

    === "Dépot"
        [https://gitlab.com/frederic-junier/parc-nsi/](https://gitlab.com/frederic-junier/parc-nsi/)

    === "Citer"
        Dépôt pour le site de  première NSI du lycée du Parc de Lyon généré avec Mkdocs
  
        Bienvenue sur le site de la classe de Première NSI du lycée du Parc à Lyon.
        Tous les documents sur ce site sont  placés sous licence :
             
        Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0).



??? note "Première NSI Gr 2 - Lycée François Mauriac - Bordeaux *Auteur : Gilles Lassus alias glassus*"
    === "Mkdocs"
        [https://glassus.github.io/premiere_nsi/](https://glassus.github.io/premiere_nsi/)

    === "Dépot"
        [https://github.com/glassus/premiere_nsi](https://github.com/glassus/premiere_nsi)

    === "Citer"        
       Lycée François Mauriac / Bordeaux : gilles.lassus [at] ac-bordeaux.fr

       L'ensemble des documents disponibles est publié sous licence CC-BY-SA



??? note "Cours de Première NSI - Groupe 1 *Auteur : M. C. Gouygou* "
    === "Mkdocs"
        [https://cgouygou.github.io/1NSI/](https://cgouygou.github.io/1NSI/)

    === "Dépot"
        [https://github.com/cgouygou/1NSI](https://github.com/cgouygou/1NSI)

    === "Citer"
        Cours de 1ere NSI - C. Gouygou

        Lycée Marguerite de Valois - Angoulême

        L'ensemble des documents disponibles est publié sous licence CC-BY-SA.


??? note " *Auteur : David Landry*"
    === "Dépot"
        Dépot uniquement.
        [https://gitlab.com/david_landry/nsi](https://gitlab.com/david_landry/nsi)

    === "Citer"
        Lycée Clemenceau à Nantes.

        Les documents déposés ici sont placés sous licence CC BY-NC-SA 3.0 FR.
        Ces documents ont été le plus souvent construits à partir de travaux de collègues ayant eu la bonne idée de placer leur travail sous une licence permettant le partage à certaines conditions.

        A la fin de chacun de mes documents, je source les auteurs qui m'ont permis de faire ces cours.

        J'en profite pour remercier chaleureusement tous les collègues partageant leur travail d'une façon ou d'une autre.
        
        David Landry


??? note "carnets.info *Auteur : Nathalie Weibel*"
    === "Mkdocs"
        [https://www.carnets.info/nsi_premiere/exposes/](https://www.carnets.info/nsi_premiere/exposes/)

    === "Dépot"
        [https://github.com/nweibel/nweibel.github.io](https://github.com/nweibel/nweibel.github.io)

    === "Citer"
        Nathalie Weibel CC BY-NC-SA 4.0 


??? note "Zone NSI *Auteur : Fabien Vergniaud*"
    === "Mkdocs"
        [https://www.zonensi.fr/](https://www.zonensi.fr/)

    === "Dépot"
        Inconnu

    === "Citer"
        Fabien Vergniaud BY



??? note "lewebpedagogique *Auteurs : d laporte*"
    === "site"
        [https://lewebpedagogique.com/dlaporte/category/nsi-1ere/](https://lewebpedagogique.com/dlaporte/category/nsi-1ere/)]

    === "Dépot"
        ?

        les cours sont sous forme de zip dans le site.

    === "Citer"
        Sauf mention contraire, tous les contenus publiés sur le WebPédagogique le sont sous le régime juridique de Creative Commons dont les principales caractéristiques sont :
  




??? note "info-mounier  *Auteur : enseignants du lycée Emmanuel Mounier à Angers*"
    === "Mkdocs"
        [https://info-mounier.fr/premiere_nsi/](https://info-mounier.fr/premiere_nsi/)

    === "Dépot"
        Inconnu

    === "Citer"
        Ces ressources ont été rédigées par les enseignants du lycée Emmanuel Mounier à Angers.

        Le contenu de ce site, sauf mention contraire, est sous licence Creative Commons BY-NC-SA 4.0 (Attribution - Pas d'utilisation commerciale - Partage dans les mêmes conditions).
  


??? note "clogique.fr *Auteur : Yves Moncheaux*"
    === "Mkdocs"
        [https://clogique.fr/nsi/premiere/](https://clogique.fr/nsi/premiere/)<br>
        [https://clogique.fr/nsi/premiere/bases_Python/](https://clogique.fr/nsi/premiere/bases_Python/)

    === "Dépot"
        Inconnu

    === "Citer"
        Yves Moncheaux
        licences : BY NC



??? note "Lycée les 3 Sources *Auteur : Romain Janvier* "
    === "Mkdocs"
        [https://nsi.janviercommelemois.fr/](https://nsi.janviercommelemois.fr/)

    === "Dépot"
        Inconnu

    === "Citer"
        Romain Janvier BY NC


??? note "mathsoup.xyz *Auteur : Inconnue*"
    === "Mkdocs"
        [http://www.mathsoup.xyz/mathsoup.xyz/index.html](http://www.mathsoup.xyz/mathsoup.xyz/index.html)

    === "Dépot"
        Inconnu

    === "Citer"
        Licence Creative Commons : CC BY 4.0
        
        Si le contenu vous intéresse, il est donc possible de le réutiliser. Ce faisant, n'oubliez pas de me citer s'il vous plaît. 

        Le site contient de nombreuses illustrations (notamment des gifs animés pour lesquels j'ai développé une étrange addiction) et dont je ne possède pas toujours les droits légaux.


??? note "mathinfo *Auteur : N. Buyle-Bodin*"
    === "Mkdocs"
        [https://www.mathinfo.ovh/Premiere_NSI/00_Progression/index.html](https://www.mathinfo.ovh/Premiere_NSI/00_Progression/index.html)

    === "Dépot"
        Inconnu

    === "Citer"
        Non renseigné.
         


      
### 1.2 Autres ressources de collègue

### 1.3 Ressources officielles
- [Éduscol](https://eduscol.education.fr/2068/programmes-et-ressources-en-numerique-et-sciences-informatiques-voie-g)



### 1.4 Autres
#### A. Vidéos YouTubes


## 2. Cours de Terminale
### 2.1 Cours, Mkdocs et dépot git de collègue

??? note "Pixees  *Auteur : David Roche*"
    === "Mkdocs"
        [https://dav74.github.io/site_nsi_term/](https://dav74.github.io/site_nsi_term/)

    === "Dépot"
        [https://github.com/dav74/site_nsi_term](https://github.com/dav74/site_nsi_term)

    === "Citer"
        David Roche, enseignant au lycée G Fichet de Bonneville (74)<br>
        [https://github.com/dav74/site_nsi_term/blob/main/LICENSE](https://github.com/dav74/site_nsi_term/blob/main/LICENSE)


??? note "eskool *Auteur : Rodrigo SCHWENCKE, colaboratif liste nsi*"
    === "Mkdocs"
        [https://eskool.gitlab.io/tnsi/](https://eskool.gitlab.io/tnsi/)

    === "Dépot"
        [https://gitlab.com/eskool/tnsi](https://gitlab.com/eskool/tnsi)

    === "Citer"
        En acceptant de publier des contenus pour votre discipline, sous Licence CC-BY-NC-SA (CC - Creative Commons, BY- Citation de l'auteur, NC - Non Commercial, SA - Share Alike - Partage dans les Mêmes Conditions)?

        Contactez-nous. Nous serons heureux de travailler avec vous, et tâcherons de développer des fonctionnalités adaptées à votre discipline.


??? note "Terminale NSI - Lycée François Mauriac - Bordeaux *Auteur : Gilles Lassus alias glassus*"
    === "Mkdocs"
        [https://glassus.github.io/terminale_nsi/](https://glassus.github.io/terminale_nsi/)

    === "Dépot"
        [https://github.com/glassus/terminale_nsi](https://github.com/glassus/terminale_nsi)

    === "Citer"        
       Lycée François Mauriac / Bordeaux : gilles.lassus [at] ac-bordeaux.fr

       L'ensemble des documents disponibles est publié sous licence CC-BY-SA


??? note "ens-fr *Auteur : Franck CHAMBON alias Francky SPOJ*"
    === "Mkdocs"
        [https://ens-fr.gitlab.io/algo2/](https://ens-fr.gitlab.io/algo2/)

    === "Dépot"
        [https://gitlab.com/ens-fr/algo2](https://gitlab.com/ens-fr/algo2)

    === "Citer"
        GNU General Public License v3.0.

        [https://gitlab.com/ens-fr/algo2/-/blob/franckySPOJ-main-patch-14377/LICENSE](https://gitlab.com/ens-fr/algo2/-/blob/franckySPOJ-main-patch-14377/LICENSE)





??? note "Cours de Terminale NSI *Auteur : M. C. Gouygou* "
    === "Mkdocs"
        [https://cgouygou.github.io/TNSI/](https://cgouygou.github.io/TNSI/)

    === "Dépot"
        [https://github.com/cgouygou/TNSI](https://github.com/cgouygou/TNSI)

    === "Citer"
        Non renseigné sur le site de terminale mais sur  le site de première :

        Cours de 1ere NSI - C. Gouygou

        Lycée Marguerite de Valois - Angoulême

        L'ensemble des documents disponibles est publié sous licence CC-BY-SA.





??? note "e-nsi *Auteur : collectif forum NSI*"
    === "Mkdocs"
        [https://e-nsi.gitlab.io/nsi-pratique/](https://e-nsi.gitlab.io/nsi-pratique/)
        [https://e-nsi.gitlab.io/pratique/](https://e-nsi.gitlab.io/pratique/)<br>
        [Présentation de e-nsi sur le forum](https://mooc-forums.inria.fr/moocnsi/login)

    === "Dépot"
        [https://gitlab.com/e-nsi/nsi-pratique](https://gitlab.com/e-nsi/nsi-pratique)
        [https://gitlab.com/e-nsi/pratique/-/tree/main/docs](https://gitlab.com/e-nsi/pratique/-/tree/main/docs)

    === "Citer"
        Exercices pratiques de NSI par Enseignants de NSI partage ou adaptation possible selon les conditions de la licence CC BY-NC-SA 4.0



??? note "Zone NSI *Auteur : Fabien Vergniaud*"
    === "Mkdocs"
        [https://www.zonensi.fr/](https://www.zonensi.fr/)

    === "Dépot"
        Inconnu

    === "Citer"
        Fabien Vergniaud BY



??? note "lewebpedagogique *Auteurs : d laporte*"
    === "site"
        [https://lewebpedagogique.com/dlaporte/nsi-cours-de-2eme-annee//](https://lewebpedagogique.com/dlaporte/nsi-cours-de-2eme-annee/)]

    === "Dépot"
        Inconnu cependant les cours sont sous forme de zip dans le site.

    === "Citer"
        Sauf mention contraire, tous les contenus publiés sur le WebPédagogique le sont sous le régime juridique de Creative Commons dont les principales caractéristiques sont :
  


??? note "mathinfo *Auteur : N. Buyle-Bodin* "
    === "Mkdocs"
        [https://www.mathinfo.ovh/Terminale_NSI/00_Progression/index.html](https://www.mathinfo.ovh/Terminale_NSI/00_Progression/index.html)

    === "Dépot"
        Inconnu

    === "Citer"
        Non renseigné.



??? note "info-mounier *Auteur : enseignants du lycée Emmanuel Mounier à Angers* "
    === "Mkdocs"
        [https://info-mounier.fr/terminale_nsi/](https://info-mounier.fr/terminale_nsi/)

    === "Dépot"
        Inconnu

    === "Citer"
        Ces ressources ont été rédigées par les enseignants du lycée Emmanuel Mounier à Angers.

        Le contenu de ce site, sauf mention contraire, est sous licence Creative Commons BY-NC-SA 4.0 (Attribution - Pas d'utilisation commerciale - Partage dans les mêmes conditions).



??? note "janviercommelemois *Auteur : Romain Janvier*"
    === "Mkdocs"
        [nsiterminale.janviercommelemois](https://nsiterminale.janviercommelemois.fr/)

    === "Dépot"
        Inconnu

    === "Citer"
        Romain Janvier BY NC


??? note "mathsoup.xyz *Auteur : ?*"
    === "Mkdocs"
        Des ressources pour les projets
        [http://www.mathsoup.xyz/mathsoup.xyz/index.html](http://www.mathsoup.xyz/mathsoup.xyz/index.html)

    === "Dépot"
        Inconnu

    === "Citer"
        Licence Creative Commons : CC BY 4.0
        
        Si le contenu vous intéresse, il est donc possible de le réutiliser. Ce faisant, n'oubliez pas de me citer s'il vous plaît. 

        Le site contient de nombreuses illustrations (notamment des gifs animés pour lesquels j'ai développé une étrange addiction) et dont je ne possède pas toujours les droits légaux.
  



??? note "impytoyable.fr *Auteur : inconue*"
    === "Mkdocs"
        Actuellement en maintenance 22/07/22

        vide pour l'instant

        [https://impytoyable.fr/index.html](https://impytoyable.fr/index.html)

    === "Dépot"
        Inconnu

    === "Citer"       
        Inconnu





### 2.2 Exercices pratiques
#### A. Mkdocs de collègue
??? note "e-nsi *Auteur : collectif forum NSI*"
    === "Mkdocs"
        [https://e-nsi.gitlab.io/nsi-pratique/](https://e-nsi.gitlab.io/nsi-pratique/)
        [https://e-nsi.gitlab.io/pratique/](https://e-nsi.gitlab.io/pratique/)<br>
        [Présentation de e-nsi sur le forum](https://mooc-forums.inria.fr/moocnsi/login)

    === "Dépot"
        [https://gitlab.com/e-nsi/nsi-pratique](https://gitlab.com/e-nsi/nsi-pratique)
        [https://gitlab.com/e-nsi/pratique/-/tree/main/docs](https://gitlab.com/e-nsi/pratique/-/tree/main/docs)

    === "Citer"
        Exercices pratiques de NSI par Enseignants de NSI partage ou adaptation possible selon les conditions de la licence CC BY-NC-SA 4.0


??? note "Pyvert_auteur : Diraison Jean "
    === "Mkdocs"
        Le site Pyvert propose 128 exercices de base d'entraînement à la programmation en langage Python.

        [https://diraison.github.io/Pyvert/](https://diraison.github.io/Pyvert/)

    === "Dépot"
        [https://github.com/diraison/Pyvert](https://github.com/diraison/Pyvert)]

    === "Citer"
        Coordonées de l'auteur disponible : [https://diraison.github.io/GenPyExo/](https://diraison.github.io/GenPyExo/)

        Le contenu est placé sous la licence CC BY-NC-SA 3.0.

  

??? note "GenPyExo_auteur : Diraison Jean "
    === "Mkdocs"
        Le site propose des exercices de programmation en Python avec vérification du programme et proposition de solution à l'issue de 9 tentatives.

        [https://diraison.github.io/GenPyExo/](https://diraison.github.io/GenPyExo/)

    === "Dépot"
        [https://github.com/diraison/GenPyExo](https://github.com/diraison/GenPyExo)

    === "Citer"
        Coordonées de l'auteur disponible : [https://diraison.github.io/GenPyExo/](https://diraison.github.io/GenPyExo/)

        Le contenu est placé sous la licence CC BY-NC-SA 3.0.

----- à traiter
- [https://mooc-forums.inria.fr/moocnsi/t/sources-md-bac-sql/6203](https://mooc-forums.inria.fr/moocnsi/t/sources-md-bac-sql/6203)

- [https://mooc-forums.inria.fr/moocnsi/t/des-qcm-en-mkdocs/6004/8](https://mooc-forums.inria.fr/moocnsi/t/des-qcm-en-mkdocs/6004/8)
- [https://clogique.fr/nsi/premiere/bases_Python/qcm/](https://clogique.fr/nsi/premiere/bases_Python/qcm/)


#### B. Jeux éducatifs

- France IOI : [France IOI](http://www.france-ioi.org/algo/chapters.php)

- Codingame : [codingame](https://www.codingame.com/multiplayer/bot-programming/great-escape)



??? note "Hackinscience *Auteur : Julien Palard* "
    === "Mkdocs"
        [https://www.hackinscience.org/exercises/](https://www.hackinscience.org/exercises/)

    === "Dépot"
        [https://framagit.org/hackinscience/hkis-website](https://framagit.org/hackinscience/hkis-website)
        [https://framagit.org/hackinscience/nsi](https://framagit.org/hackinscience/nsi)

    === "Citer"
        MIT [https://framagit.org/hackinscience/hkis-website/-/blob/master/LICENSE](https://framagit.org/hackinscience/hkis-website/-/blob/master/LICENSE)



---- à traiter
[https://mooc-forums.inria.fr/moocnsi/t/pyrates-debuter-ou-reprendre-la-programmation-python/3276](https://mooc-forums.inria.fr/moocnsi/t/pyrates-debuter-ou-reprendre-la-programmation-python/3276)

### 2.3 Ressources officielles

- [Éduscol](https://eduscol.education.fr/2068/programmes-et-ressources-en-numerique-et-sciences-informatiques-voie-g)
  
- Sujet Examen du bac 2022 : [https://www.education.gouv.fr](https://www.education.gouv.fr/reussir-au-lycee/bac-2022-les-sujets-des-epreuves-ecrites-de-specialite-341303)
  

### 2.4 Vidéo youtubes


### 2.5 Ressources exam
??? note "e-nsi *Auteurs : collectif forum NSI"
    === "Mkdocs"
        Sujets épreuve écrite : [https://e-nsi.gitlab.io/ecrit/](https://e-nsi.gitlab.io/ecrit/)

        s'exercer à l'épreuve pratique : [https://e-nsi.gitlab.io/pratique/](https://e-nsi.gitlab.io/pratique/)


    === "Dépot"
        [https://gitlab.com/e-nsi/ecrit](https://gitlab.com/e-nsi/ecrit)

    === "Citer"
        Exercices pratiques de NSI par Enseignants de NSI partage ou adaptation possible selon les conditions de la licence CC BY-NC-SA 4.0


??? note "Terminale NSI - Lycée François Mauriac - Bordeaux *Auteur : Gilles Lassus alias glassus*"
    === "Mkdocs"
        [https://glassus.github.io/terminale_nsi/T6_Annales/epreuve_pratique/](https://glassus.github.io/terminale_nsi/T6_Annales/epreuve_pratique/)

    === "Dépot"
        [https://github.com/glassus/terminale_nsi](https://github.com/glassus/terminale_nsi)


    === "Citer"
        Lycée François Mauriac / Bordeaux : gilles.lassus [at] ac-bordeaux.fr
        
        L'ensemble des documents disponibles est publié sous licence CC-BY-SA


??? note "Pixees  *Auteur : David Roche*"
    === "Mkdocs"
        Épreuve pratique : [https://pixees.fr/informatiquelycee/term/ep/index.html](https://pixees.fr/informatiquelycee/term/ep/index.html)

        Épreuve écrite :[https://pixees.fr/informatiquelycee/term/suj_bac/](https://pixees.fr/informatiquelycee/term/suj_bac/)

    === "Dépot"
        [https://github.com/dav74/site_nsi_term](https://github.com/dav74/site_nsi_term)

    === "Citer"
        David Roche, enseignant au lycée G Fichet de Bonneville (74)<br>
        [https://github.com/dav74/site_nsi_term/blob/main/LICENSE](https://github.com/dav74/site_nsi_term/blob/main/LICENSE)



??? note "  *Auteurs : Vincent-Xavier JUMEL*"
    === "Mkdocs"
        exite mais pas trouvé

    === "Dépot"
        [https://gitlab.com/vincentxavier/nsi/-/tree/main/docs/2022](https://gitlab.com/vincentxavier/nsi/-/tree/main/docs/2022)

    === "Citer"
        ?

??? note "Sujets et corrigé sur un blog lewebpedagogique *Auteurs : d laporte*"
    === "site"
        [sujets et corrigé 1](https://lewebpedagogique.com/dlaporte/corrections-epreuves-pratiques-2021-nsi/)

    === "Dépot"
        Inconnu

    === "Citer"
        Sauf mention contraire, tous les contenus publiés sur le WebPédagogique le sont sous le régime juridique de Creative Commons dont les principales caractéristiques sont :
  


- Sujet écrit 2022 : [https://www.education.gouv.fr](https://www.education.gouv.fr/reussir-au-lycee/bac-2022-les-sujets-des-epreuves-ecrites-de-specialite-341303)


### 2.6 Autres
#### A. Chaines YouTubes
- [À la découverte des graphes](https://www.youtube.com/channel/UCHtJVeNLyR1yuJ1_xCK1WRg)

- [Informatique théoriques](https://www.youtube.com/channel/UC4m4TCpEwpHSxmwr3zdkGXQ/videos)

- [Wandida, EPFL (plein de super vidéo de cours)](https://www.youtube.com/watch?v=clZ4q5zPBlE)


#### B. Blogs, sites

- [Interstices](https://interstices.info/)
  
Un blog avec des articles assez pointu (sur certaine notion il m'a permis de comprendre)

- [Sametmax](https://sametmax2.com/ce-quil-faut-savoir-en-python/index.html)







## 4. Ressources pour prof
### 4.1 Forum

- [Forum NSI](https://mooc-forums.inria.fr/moocnsi/t/classe-de-1ere-partagee-spe-nsi/6001/6)



### 4.2 MOOC NSI

??? note "Mooc nsi sur fun *Auteur : collectifs*"
    === "Site"
        [https://www.fun-mooc.fr/fr/cours/numerique-et-sciences-informatiques-les-fondamentaux/](https://www.fun-mooc.fr/fr/cours/numerique-et-sciences-informatiques-les-fondamentaux/)

    === "Dépot"
        - [https://gitlab.inria.fr/learninglab/nsi-capes-info](https://gitlab.inria.fr/learninglab/nsi-capes-info)
        - [https://gitlab.com/vincentxavier/mooc-2-ressources](https://gitlab.com/vincentxavier/mooc-2-ressources)

    === "Citer"
        Auteurs et licences: [https://www.fun-mooc.fr/fr/cours/numerique-et-sciences-informatiques-les-fondamentaux/](https://www.fun-mooc.fr/fr/cours/numerique-et-sciences-informatiques-les-fondamentaux/)

        Licence globalement : BY NC


        







#### 4.3 Liens ressources de cours DIU EIL

??? note "DIU au Havre *Auteur : Bruno Mermet*"
    === "Site"
        [https://mermet.users.greyc.fr/Enseignement/EnseignementInformatiqueLycee/Havre/](https://mermet.users.greyc.fr/Enseignement/EnseignementInformatiqueLycee/Havre/)

    === "Dépot"
        Inconnu

    === "Citer"
        Bruno Mermet — Université Le Havre - Normandie, Licence Creative Commons BY-NC-SA, 2020


??? note "univ-lyon1 "
    === "Site"
        [https://diu-eil.univ-lyon1.fr](https://diu-eil.univ-lyon1.fr)

    === "Dépot"
        Inconnu


----

???+ note "_auteur : ? "
    === "Mkdocs"
        [jussieu](http://www-licence.ufr-info-p6.jussieu.fr/lmd/licence/2018/ue/DIU-EIL-2019jun/index.php 8)

    === "Dépot"
        ?

    === "Citer"
        ??



???+ note "_auteur : ? "
    === "Mkdocs"
        [b](http://deptmedia.cnam.fr/new/spip.php?rubrique774 7)

    === "Dépot"
        ?

    === "Citer"
        ??





???+ note "_auteur : ? "
    === "Mkdocs"
        ?
    === "Dépot"
        [lille](https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/tree/master 8) 
        [lille](https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/blob/master/Readme.md)

    === "Citer"
        ?



???+ note "_auteur : ? "
    === "Mkdocs"
        
    === "Dépot"
        [bordeaux](https://diu-uf-bordeaux.github.io/bloc1/ 10)


    === "Citer"
        ??



???+ note "_auteur : ? "
    === "Mkdocs"
        ?

    === "Dépot"
        [bordeaux](https://github.com/diu-uf-bordeaux 6)

    === "Citer"
        ?



???+ note "_auteur : ? "
    === "Mkdocs"
        ?

    === "Dépot"
        [grenoble alpes](https://diu-eil.gricad-pages.univ-grenoble-alpes.fr/ 6)?

    === "Citer"
        ??



???+ note "_auteur : ? "
    === "Mkdocs"
        [b](https://diu.lri.fr/ 11)

    === "Dépot"
        ?

    === "Citer"
        ??



???+ note "_auteur : ? "
    === "Mkdocs"
        [b](https://www.lri.fr/~kn/diu-nsi_en.html 5)

    === "Dépot"
        ?

    === "Citer"
        ??



???+ note "_auteur : ? "
    === "Mkdocs"
        [paris13](https://lipn.univ-paris13.fr/~breuvart/DIU-EIL/ 5)

    === "Dépot"
        ?

    === "Citer"
        ??


???+ note "_auteur : ? "
    === "Mkdocs"
        [b](https://gitlab.com/GiYoM/du 9)

    === "Dépot"
        ?

    === "Citer"
        ??



???+ note "_auteur : ? "
    === "Mkdocs"
        [b](https://pageperso.lis-lab.fr/~benjamin.monmege/diu-eil-amu 8)

    === "Dépot"
        ?

    === "Citer"
        ??



???+ note "univ-antilles_auteur : ? "
    === "Mkdocs"
        [univ-antilles](https://ecursus.univ-antilles.fr/course/view.php?id=2389 10)

    === "Dépot"
        ?

    === "Citer"
        ??




???+ note "_auteur : ? "
    === "Mkdocs"
        [https://www.enseignement.polytechnique.fr/informatique/INF412/i.php?n=Main.Poly](https://www.enseignement.polytechnique.fr/informatique/INF412/i.php?n=Main.Poly)

    === "Dépot"
        ?

    === "Citer"
        ??




### 4.4 Cours de prépa, fac

??? note " *Auteur : Gérard Lavau* "
    === "Mkdocs"
        [https://lavau.pagesperso-orange.fr/cpge/cpge1/](https://lavau.pagesperso-orange.fr/cpge/cpge1/)
    
    === "pdf"
        [https://lavau.pagesperso-orange.fr/mpcsi13/algo1.pdf](https://lavau.pagesperso-orange.fr/mpcsi13/algo1.pdf)

    === "Dépot"
        Inconnu

    === "Citer"
        Le cours figurant sur ce site est soumis au droit d'auteur. Cela signifie en particulier que l'auteur dispose des deux droits suivants :

        Le droit de REPRÉSENTER son œuvre. Cette représentation est la communication au public de l'œuvre. Elle consiste dans le cas présent à la mise à disposition du cours sur ce site internet et uniquement sur celui-ci. Toute autre présentation de l'œuvre sur un autre site, un blog ou sur tout autre espace de stockage accessible à distance à d'autres que vous est INTERDITE. Par conséquent, si vous êtes le gestionnaire d'un site sur Internet ou si vous disposez d'un blog ou d'un espace de stockage accessible à distance à d'autres que vous, vous avez le droit de créer un lien de votre site vers mon site ou vers des fichiers de mon site, à condition que ce lien soit accessible librement et gratuitement. VOUS NE POUVEZ PAS télécharger les fichiers de mon site pour les installer sur le vôtre.

        Le droit de REPRODUIRE son œuvre. Dans le cas présent, cette reproduction consiste en la sauvegarde des fichiers PDF du cours sur un disque dur personnel ou dans un espace de stockage privé accessible à vous seul, en l'impression de ce cours sur imprimante à partir de ces fichiers, ou en la photocopie du cours préalablement imprimé. Ces trois modes de reproduction sont actuellement librement AUTORISÉS par l'auteur. Ainsi, VOUS AVEZ TOUTE LIBERTÉ pour télécharger, imprimer, photocopier ce cours et le diffuser gratuitement. Toute diffusion à titre onéreux ou utilisation commerciale est interdite sans accord de l'auteur.

        Si vous souhaitez faire un geste de remerciement envers l'auteur, vous pouvez m'envoyer une photographie de votre ville par courrier électronique. Je vous en serai infiniment reconnaissant.





### 4.5 Liens CAPES/AGREG NSI

???+ note "_auteur : ? "
    === "Mkdocs"
        [capes-nsi.org](https://capes-nsi.org/ressources/)

    === "Dépot"
        ?

    === "Citer"
        ??






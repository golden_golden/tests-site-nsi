# Ressources SNT




## 1 Mkdocs et dépot git de collègue



??? note "Pixees *Auteur : David Roche*"
    === "Mkdocs"
        [https://pixees.fr/informatiquelycee/sec/index.html](https://pixees.fr/informatiquelycee/sec/index.html)

    === "Dépot"
        [https://github.com/dav74/site_snt](https://github.com/dav74/site_snt)

    === "Citer"
        David Roche, enseignant au lycée G Fichet de Bonneville (74)<br>
        C0 [https://github.com/dav74/site_nsi_term/blob/main/LICENSE](https://github.com/dav74/site_nsi_term/blob/main/LICENSE)


??? note "SNT en Seconde *Auteur : M. C. Gouygou*"
    === "Mkdocs"
        [https://cgouygou.github.io/2SNT/](https://cgouygou.github.io/2SNT/)

    === "Dépot"
        [https://github.com/cgouygou/2SNT](https://github.com/cgouygou/2SNT)

    === "Citer"
        Cours de SNT en Seconde - C. Gouygou

        Lycée Marguerite de Valois - Angoulême

        L'ensemble des documents disponibles est publié sous licence CC-BY-SA.



??? note "SNT 207 / 215 - Lycée François Mauriac - Bordeaux *Auteur : Gilles Lassus alias glassus*"
    === "Mkdocs"
        [https://glassus.github.io/snt/](https://glassus.github.io/snt/)

    === "Dépot"
        [https://github.com/glassus/snt](https://github.com/glassus/snt)

    === "Citer"        
       Lycée François Mauriac / Bordeaux : gilles.lassus [at] ac-bordeaux.fr

       L'ensemble des documents disponibles est publié sous licence CC-BY-SA



???+ note "eskool *Auteur : Rodrigo SCHWENCKE*"
    === "Dépot"
        [https://gitlab.com/eskool/profs-info/snt-public](https://gitlab.com/eskool/profs-info/snt-public)

    === "Citer"
        ??



???+ note "info-mounier *Auteurs : enseignants du lycée Emmanuel Mounier à Angers* "
    === "Mkdocs"
        [https://info-mounier.fr/snt/](https://info-mounier.fr/snt/)

    === "Dépot"
        Inconnu

    === "Citer"
        Ces ressources ont été rédigées par les enseignants du lycée Emmanuel Mounier à Angers.

        Le contenu de ce site, sauf mention contraire, est sous licence Creative Commons BY-NC-SA 4.0 (Attribution - Pas d'utilisation commerciale - Partage dans les mêmes conditions).




???+ note "mathinfo *Auteurs : N. Buyle-Bodin* "
    === "Mkdocs"
        [https://www.mathinfo.ovh/SNT_2nde/index_general.html](https://www.mathinfo.ovh/SNT_2nde/index_general.html)

    === "Dépot"
        ?

    === "Citer"
        ??


???+ note "janviercommelemois *Auteur : Romain Janvier*"
    === "Mkdocs"
        [nsiterminale.janviercommelemois](https://snt.janviercommelemois.fr/)

    === "Dépot"
        Inconnu

    === "Citer"
        Romain Janvier BY NC


???+ note "Zone NSI *Auteur : F.Vergniaud*"
    === "Mkdocs"
        [https://www.zonensi.fr/](https://www.zonensi.fr/)

    === "Dépot"
        Inconnu

    === "Citer"
        F.Vergniaud BY


???+ note "SNT *Auteur : Louis Paternault*"
    === "Mkdocs"
        [snt.ababsurdo.fr-liens ressources](https://snt.ababsurdo.fr/prof/)

    === "Dépot"
        Inconnu

    === "Citer"
        2019-2023 Louis Paternault — Publié sous licence Creative Commons by-sa 4.0. 



???+ note "icnisnlycee *Auteur : Julien Launay*"
    === "Mkdocs"
        [icnisnlycee](http://icnisnlycee.free.fr/index.php/42-snt)

    === "Dépot"
        Inconnu

    === "Citer"
        Julien Launay


???+ note "SITE SNT - ELLA *Auteur : Stéphane Colomban*"
    === "Mkdocs"
        [SITE SNT - ELLA](https://snt.entraide-ella.fr)

    === "Dépot"
        Inconnu

    === "Citer"
        Stéphane Colomban



???+ note "SNT web *Auteur : Julien de Villele*"
    === "Mkdocs"
        [SNT web](http://prof.devillele.free.fr/)

    === "Dépot"
        Inconnu

    === "Citer"
        Julien de Villele


???+ note "ENT Lannemezan *Auteur : Nicolas Tourreau*"
    === "Mkdocs"
        [Nicolas Tourreau](http://citescolaire-lannemezan.mon-ent-occitanie.fr/disciplines/sciences-numeriques-et-technologie/)

        [fiches connaissances en SNT](https://citescolaire-lannemezan.mon-ent-occitanie.fr/disciplines/sciences-numeriques-et-technologie/fiches-connaissances-51467.htm)

    === "Dépot"
        Inconnu

    === "Citer"
        Nicolas Tourreau


------



???+ note "ENT *Auteur : N*"
    === "Mkdocs"
        [Pascal Rousset](https://drive.google.com/drive/folders/1mh906RDyeWJfEMLIZbUghjlDq4Lqhs7u?usp=sharing)

    === "Dépot"
        Inconnu

    === "Citer"
        Inconnu

-----

???+ note "ENT *Auteur : N*"
    === "Mkdocs"
        [Gilles Lassus](https://drive.google.com/drive/folders/1G8zVpO3Lv2Ucg8-uNVRFiBltWMQLTtIm)

    === "Dépot"
        Inconnu

    === "Citer"
        Inconnu




???+ note "ENT *Auteur : N*"
    === "Mkdocs"
        [Pierrick Vaire](http://pierrick.vaire.free.fr/snt/)

    === "Dépot"
        Inconnu

    === "Citer"
        Inconnu




???+ note "ENT *Auteur : N*"
    === "Mkdocs"
        [Julien Chu, Cédric Lusseau, Fadi Tamin](https://clusseau-drive.mycozy.cloud/public?sharecode=Qu6dggO4Ej3o)

    === "Dépot"
        Inconnu

    === "Citer"
        Inconnu



???+ note "ENT *Auteur : N*"
    === "Mkdocs"
        [IREM de la Réunion](http://irem.univ-reunion.fr/spip.php?article1022)

    === "Dépot"
        Inconnu

    === "Citer"
        Inconnu




???+ note "ENT *Auteur : N*"
    === "Mkdocs"
        [Activités de médiation de Marie Duflot](https://members.loria.fr/MDuflot/files/med/index.html)

    === "Dépot"
        Inconnu

    === "Citer"
        Inconnu




???+ note "ENT *Auteur : N*"
    === "Mkdocs"
        [Christophe Mieszczak](https://framagit.org/tofmzk/informatique_git/-/blob/master/snt/readme.md)

    === "Dépot"
        Inconnu

    === "Citer"
        Inconnu





## 2 Autres ressources de collègue
- [https://sntsecondecimf.wordpress.com/](https://sntsecondecimf.wordpress.com/)
- [snt.ababsurdo.fr_pages de ressources ](https://snt.ababsurdo.fr/prof/)
- [sites de ressources](http://www.pearltrees.com/t/snt/id23193114)
  
## 3 Ressources officielles

- [eduscol.education](https://eduscol.education.fr/cid143713/snt-bac-2021.html)
  
## 4 Vidéo youtubes

## 5 Autres
- [Partage de ressources_liste de diffusion snt](https://tribu.phm.education.gouv.fr/portal/auth/pagemarker/434/cms/default-domain/workspaces/enseignants-sciences-numeriques-technologie-snt-national)
  
- [https://irem.univ-reunion.fr/spip.php?rubrique205](https://irem.univ-reunion.fr/spip.php?rubrique205)



#### Autres ressources
- Une [liste de diffusion](https://groupes.renater.fr/sympa/info/sciences-numeriques-technologie) d'enseignants de SNT.

- Le [MOOC SNT](https://www.fun-mooc.fr/courses/course-v1:inria+41018+session01/about).


- [Des liens pour la SNT](http://www.pearltrees.com/t/snt/id23193114)
- [Interstices](https://interstices.info/sciences-numeriques-et-technologie-au-lycee/)
- Ressources proposées par [FFDN](https://www.ffdn.org/wiki/doku.php?id=formations:seconde).
- Voir aussi la [bibliographie](/biblio).
- La revue [*Au Fil des maths*](https://afdm.apmep.fr) de [l'APMEP](https://www.apmep.fr/) propose plusieurs activités de SNT :
- [Les petits papiers](https://afdm.apmep.fr/rubriques/fil/activite-snt-les-petits-papiers)
- [Le robot sauteur](https://afdm.apmep.fr/rubriques/fil/activite-snt-le-robot-sauteur/)
- et d'autres… ?




### 5.x Présentation de la SNT



----


### À traiter

[https://linuxfr.org/users/purplepsycho/journaux/openstreetmap-et-gps-garmin](https://linuxfr.org/users/purplepsycho/journaux/openstreetmap-et-gps-garmin)

??? note "convestation liste de diffusion"
        Absolument génial, les élèves ont modélisé, abstrait, cherché des motifs et même un peu abordé la récursivité : http://revue.sesamath.net/spip.php?article1531

        Thèmes abordés (surtout sur les piliers langages et machines) :

        * géolocalisation (lien entre repérages par coordonnées et par orientation - repère de Frénet)
        * robotique (la tortue est un robot simulé ; la suite serait de refaire ça avec un robot style Ti Rover, Thymio etc)
        * image numérique (on fait des dessins)
        * Python : boucles finies, type string, séquence d'instructions, fonctions (celles de la tortue), variables...
        * histoire (pas celle de l'informatique, mais celle des maths et des fractales, et Sierpinski était cryptographe qui a probablement formé Rejewski...)

        Du point de vue didactique ce serait intéressant à analyser parce que le changement de cadre a été central dans cette activité (lien entre le dessin, le texte et le programme Python).

        Bonne lecture

        Alain

        PS je voulais enchaîner sur une activité Pyxel art mais malgré la reconquête du mois de juin j'ai pas eu le temps... 


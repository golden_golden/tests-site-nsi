# Liens chaînes YouTube


#### Fred Leleu (Vidéo sur Mkdocs):
- [Tutoriel MKdocs](https://www.youtube.com/playlist?list=PL-Q7fIakgvUAcUluPeUMIP1128wWxboJY)


#### Graven :

bien comme inspiration notamment pour des gros projets: jeu vidéo, site web, …

il fait également des petites vidéo sur le langage python assez sympa

- [Graven](https://www.youtube.com/channel/UCIHVyohXw6j2T-83-uLngEg)

ps : il n'y a pas que du python.




#### David Latouche
Chaîne d'un collègue, les vidéos sont parfaitement en accord avec les programmes !
- [David Latouche](https://www.youtube.com/c/DavidLatouche/videos)





#### Machine learnia :
Les premières vidéo sur les bases du langage python sont excellentes.

- [Machine learnia](https://www.youtube.com/c/MachineLearnia/videos)




#### FormationVidéo :
Des petites vidéo sur les notions python.

- [FormationVidéo](https://www.youtube.com/watch?v=HWxBtxPBCAc&list=PLrSOXFDHBtfHg8fWBd7sKPxEmahwyVBkC)




#### xavski :
Un peu tout les sujets

- [xavski](https://www.youtube.com/c/xavki-linux)
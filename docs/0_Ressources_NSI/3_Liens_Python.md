# Lien Python


## 1. Sites avec du cours python :
- [Couspython.com](https://courspython.com/introduction-python.html)

- [python.doctor](https://python.doctor/page-comprehension-list-listes-python-cours-debutants)

- [https://python.sdv.univ-paris-diderot.fr/](https://python.sdv.univ-paris-diderot.fr/)


- [https://wiki.python.org/moin/FrenchLanguage](https://wiki.python.org/moin/FrenchLanguage)


## 2. Vidéos sur python
#### Cours python 3

- [Cours python 3](https://www.youtube.com/channel/UCIlUBOXnXjxdjmL_atU53kA/videos)
  
!!! note "remarque"
    Ce sont les vidéo qui corresponde au cours Mooc de la plateforme Fun, intitulé Python 3.

#### Docstring
- [Docstring](https://www.youtube.com/c/Docstring/videos)
- 
!!! note "remarque"
    Des vidéos sur le langage python.
    Bien faites en général, certaines vidéo ont des formats super intéressant pour de la diffusions en classe.


## 3. Memento, Cheatsheet, …
- [https://perso.limsi.fr/pointal/_media/python:cours:mementopython3.pdf](https://perso.limsi.fr/pointal/_media/python:cours:mementopython3.pdf)
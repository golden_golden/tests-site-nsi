# Club Informatique


## Club informatique :
### Pourquoi un club informatique ?
Hé oui, on est bien d’accord, l’objectif « caché » est de booster les effectifs de 1ère NSI !

### Pour quel public ?
Pour les élèves de niveau seconde. 



## Information issus de la liste de diffusions :

-----
!!! note "message 1"
    Bonjour,

    Après toutes les années d'ISN / ICN et compagnie, voici ce que j'ai retenu et qui marchait bien :
    Le premier "projet" est un tutoriel qu'on fait tous en même temps. Le prof au tableau, projette et tape le code, ils recopient bêtement. 
    C'est un snake qu'on construit progressivement en partant d'un rendu générique avec une grille déjà dessinée.
    Les étapes sont détaillées en ligne, pour ceux qui avancent vite. Généralement, on rattrape tout le monde.


    Ainsi ils découvrent les syntaxes de base, comprennent au fur et à mesure comment les éléments interagissent.

    Ça dure 2h à peu près. Parfois 3 s'ils sont vraiment novices. Ça aura aussi l'immense avantage de faire fuir ceux qui viennent pour jouer aux jeux-vidéos.

    Une fois cette étape franchie, ils ont un projet à faire évoluer s'ils veulent ou peuvent repartir d'une des étapes pour un futur projet.

    Ensuite les concepts de base de la pédagogie de projet s'appliquent :

        limiter la taille des groupes,
        imposer le langage et les librairies,
        s'assurer d'un suivi régulier,
        avancer petit à petit.
        refuser les projets trop difficiles pour eux (genre Pacman + déplacement de tous les fantomes)


    ---

    Avant d'arriver à ça, je les laissais suivre un tuto rédigé pour créer pong. Mais la géométrie pose des problèmes à beaucoup et les trajectoires de la balle sont délicates à debugger pour les élèves.
    Certains y passaient des lustres et n'en finissaient pas.
    Encore avant, je faisais faire un mini projet graphique en JS (genre score du tennis avec 2 boutons : joueur1 a marqué, joueur2 a marqué). Ce n'était pas très formateur.

    ---

    Je recommande fortement d'utiliser Pygame Zero plutôt que Pygame seul.
    L'interface est grandement simplifiée et permet néanmoins de créer des jeux sympas.
    L'inconvénient majeur est qu'avant la POO, il est difficile de se passer de variables globales.

    Amusez-vous bien !
    QK

----

!!! note "message 2"
    Le 24/06/2022 à 13:37, quentin konieczko (via numerique-sciences-informatiques Mailing List) a écrit :
    > Bonjour,
    >
    > Après toutes les années d'ISN / ICN et compagnie, voici ce que j'ai retenu et qui marchait bien :
    > Le premier "projet" est un tutoriel qu'on fait tous en même temps. Le prof au tableau, projette et tape le code, ils recopient bêtement. 
    > C'est un snake qu'on construit progressivement en partant d'un rendu générique avec une grille déjà dessinée.
    > Les étapes sont détaillées en ligne, pour ceux qui avancent vite. Généralement, on rattrape tout le monde.

    Bonjour, c'est amusant, je faisais la même chose ! Jusqu'à des STI qui, l'an passé, ont réclamé qu'on finisse l'année par 2 heures sur le sujet. Ce qui me fait penser que je n'ai toujours pas fini de monter la vidéo capturée à cette occasion !


    > Ainsi ils découvrent les syntaxes de base, comprennent au fur et à mesure comment les éléments interagissent.
    >
    > Ça dure 2h à peu près. Parfois 3 s'ils sont vraiment novices. Ça aura aussi l'immense avantage de faire fuir ceux qui viennent pour jouer aux jeux-vidéos.
    >
    > Une fois cette étape franchie, ils ont un projet à faire évoluer s'ils veulent ou peuvent repartir d'une des étapes pour un futur projet.
    >
    > Ensuite les concepts de base de la pédagogie de projet s'appliquent :
    >
    >     limiter la taille des groupes,
    >     imposer le langage et les librairies,
    >     s'assurer d'un suivi régulier,
    >     avancer petit à petit.
    >     refuser les projets trop difficiles pour eux (genre Pacman + déplacement de tous les fantomes)
    >
    Je valide totalement, sauf un point : pour moi, il me semble essentiel, « au début » au moins, que les jeunes « fassent seuls ». Il faut avoir une idée des difficultés qu'on peut rencontrer, s'y confronter en totalité et parvenir à les surmonter (pas nécessairement tout seul, l'entraide est naturellement permise). Un projet à plusieurs, c'est bien, mais ça ajoute d'autres difficultés : chaque chose en son temps.


    > Je recommande fortement d'utiliser Pygame Zero plutôt que Pygame seul.
    > L'interface est grandement simplifiée et permet néanmoins de créer des jeux sympas.
    > L'inconvénient majeur est qu'avant la POO, il est difficile de se passer de variables globales.

    C'est là l'occasion rêvée de faire intervenir le type « dictionnaire » pour gérer tous les paramètres du jeu. C'est un peu lourd mais :

        c'est formateur pour la maîtrise de l'objet ;
        c'est mutable -> plus besoin de global !

    TL




!!! note "idée de projet"
    Je me permets de faire le lien ici avec la proposition d’atelier « Maker » proposé la semaine dernière :

    Sur le même principe, je comptais proposer comme activité de réalisation une version simplifiée du jeu de SIMON.
    Reproduire une séquence sonore et lumineuse déterminée aléatoirement.

    Ce jeu peut soit être uniquement sur écran, soit associé à une platine avec un microcontrôleur ou Raspberry-Pi.
    Je vous mets en pièce jointe une photo du prototype avec un ESP8266 (coût total moins de 6€)  D’accord il y a un peu de soudure mais ça reste très abordable.

    Du coup j’espère toucher à la fois celles et ceux qui souhaiteraient 
    - faire un peu de techno,
    - coder et avoir un objet avec lequel on peut interagir

    Exemple trois versions :
    - seconde -> pas de liste
    - 1ere NSI -> une version  liste une autre dictionnaire
    - term -> POO
  
    Jacques




!!! note ""
        Bonjour, 
        J'essaie d'animer un atelier "Code Décode" dans mon lycée depuis deux ans.
        Au départ, l'idée était de proposer des entraînements pour les divers petits concours existants : Castor, Algoréa, Coding UP ... 
        En fait nous avons aussi dérivé vers l'apprentissage du codage en lui même, la programmation de microcontrôleurs (Microbit et ruban led par exemple) et en terme de challenge de programmation, je vais m'orienter davantage vers la Nuit du Code, qui est une belle proposition.

        Concernant la programmation, il y a un site : pydéfis, qui est très bien et qui offre une belle quantité de petites énigmes contextualisées, de difficulté graduées issues du concours CodingUP. Les élèves ont moins accroché cette année que les années passées, mais pour les élèves déjà un peu à l'aise dans le codage, c'est une bonne ressource.

        Bonne fin de semaine, Marianne Seddoh



!!! note ""
    Bonjour Marianne
    Merci pour ton témoignage. Je connais Pydéfis et effectivement c’est très sympa. Mais je ne connaissait pas Coding Up, je vais regarder de plus près.

    Pour Castor et Algoréa, ils en font déjà en SNT, donc je voudrais proposer autre chose.

    Plus j’y pense, plus je me dis que je vais essayer de brancher mon établissement sur la Nuit du Code, en motivant les profs de collège pour la version Scratch, et la profs d’arts plastiques pour la déco : ça peut être un projet vraiment chouette pour tout l’établissement, en donnant vraiment une tonalité « année du numérique »

    Bravo d’ailleurs à l’équipe de la Nuit du Code pour l’énorme boulot fait sur Edupyter et Pyxel !!


### Synthèse : 
1) 
- commencer par des mini-projets que les élèves peuvent faire seuls et qui ne soient pas forcément des jeux (exemples qui ont bien marché : chiffres romains, ou cowsay : https://blog.univ-reunion.fr/academieisn/ etc)
   
1) favoriser les dictionnaires dans le cadre du projet parce que je pense qu'un.e élève retiendra mieux une solution à un problème rencontré en projet, qu'un cours magistral

3)J'ajouterais bien un troisième point : combiner le projet avec une découverte de l'informatique embarquée style micro:bit ou Sense Hat : https://irem.univ-reunion.fr/spip.php?article889



### Lien sympa : 
 https://gdevelop.io/fr-fr  (libre de droit et version online aussi) car programmation sans python mais avec des boucles et des déclencheurs et selon leurs avancements.

-  Pyxel (https://github.com/kitao/pyxel) : 
        Comme ça, les élèves pourront aussi   participer à la Nuit du c0de (https://www.nuitducode.net).
        Pour la 6e édition de la Nuit du c0de, Python / Pyxel a été proposé (en plus de Scratch). Gros succès!
        Quelques ressources (qui vont s'étoffer) : https://nuitducode.github.io/DOCUMENTATION/PYTHON/01-presentation/
        Laurent

-   Pygame Zero, Pygame, (ou même Unity).


### Exemples de jeux
[https://www.youtube.com/watch?v=N470mH0Xz8Y](https://www.youtube.com/watch?v=N470mH0Xz8Y)
[https://www.youtube.com/watch?v=W-QOtdD3qx4](https://www.youtube.com/watch?v=W-QOtdD3qx4)
[snake](https://www.youtube.com/watch?v=WVWHLttbUvI)

### Chaine youtube codage d'un jeu
[https://www.youtube.com/watch?v=clcmhmpyYSc](https://www.youtube.com/watch?v=clcmhmpyYSc)
[https://www.youtube.com/channel/UCHLoH472O3UyDEa2B5TQs-A/videos](https://www.youtube.com/channel/UCHLoH472O3UyDEa2B5TQs-A/videos)
[https://www.youtube.com/watch?v=FQDgIAJ_Otw](https://www.youtube.com/watch?v=FQDgIAJ_Otw)
[https://www.youtube.com/watch?v=PGsuOm8PL_c](https://www.youtube.com/watch?v=PGsuOm8PL_c)

### autre idée de projets
[https://www.youtube.com/watch?v=8ext9G7xspg](https://www.youtube.com/watch?v=8ext9G7xspg)




### autre script python dans des logiciels
[https://wiki.freecadweb.org/Scripting_and_macros/fr](https://wiki.freecadweb.org/Scripting_and_macros/fr)
# Une basse MkDocs, pour la réalisation de cours de NSI

Cette base prend en charge :

- Pyodide : Un IDE et une console python intégrée et fonctionnant en local.
  
- Mkdocs-SQLite-Console :  Pour pouvoir travailler sur une basse de donnée SQLite et s'exercer à faire de requêtes.
  
- Mermaid et kroki : Pour la création de nombreuse représentation graphique *(Diagramme de classe, graphes, arbres,…)*

- QCM : Pour concevoir des QCM.


!!! note "Travail en cours"

    - PDF : Une feuille de style css spéciale pour le print permet d'obtenir un cours tout propre lorsque l'on utilise la fonction impression de son navigateur.




<br/>

## Récupération du Dépot
Le dépôt correspondant au site que vous avez actuellement sous les yeux est à [cloner](https://gitlab.com/golden_golden/tests-site-nsi) et adapter à votre besoin.

<center>[Télécharger l'archive .zip](https://gitlab.com/golden_golden/tests-site-nsi/-/archive/main/tests-site-nsi-main.zip){ .md-button }</center>


<br/>


## Visualiser le site
### Utilisation de Docker
#### Pourquoi utiliser Docker ?
Afin de garantir un fonctionnement identique en local et sur GitLab, j'ai choisi d'utiliser des images ***Docker*** .

Le fait d'utiliser les images Docker bloque les versions des plugins utilisés et garantie que le site restera fonctionnel pendant toute l'année scolaire.

Pour en savoir plus sur Docker et notamment comment l'installer : [Docker](./0_Outils/20-Docker.md)

<br/>

#### Récupérer les images Docker

En lien avec ce site, il existe deux images docker:

L'image `prod`, utilisé par le `gitlab.ci` :
> <center>[Télécharger l'image Docker `prod` ](https://hub.docker.com/r/goldengolden28/mkdocs-material-plugins-nsi-prod){ .md-button }</center>

<br/>

Et l'image `dev`, qui est en réalité la même image mais qui lance automatiquement le serveur Mkdocs sur le port 8000, ce qui permet de gagner un peu de temps lors de l'utilisation.

> <center>[Télécharger l'image Docker `dev` ](https://hub.docker.com/r/goldengolden28/mkdocs-material-plugins-nsi-dev){ .md-button }</center>

Ces images comportent tout les plugins nécessaires au bon fonctionnement du site que vous avez actuellement sous les yeux.<br>

??? note "Lien relatifs à ces images"
    L'ensemble des documents permettant la création de ces deux images est disponible sur mon gitlab : 
    [https://gitlab.com/golden_golden/docker_mkdocs_nsi](https://gitlab.com/golden_golden/docker_mkdocs_nsi)



<br/>





#### Lancer le container Docker dans un terminal

Pour lancer l'image Docker, il suffit de se placer dans le dossier contenant le fichier `mkdocs.yml`, d'ouvrir un terminal et de saisir la ligne de commande suivante.

```shell
    docker run --rm -it -p 8000:8000 -v ${PWD}:/docs goldengolden28/mkdocs-material-plugins-nsi-dev:latest  
```

Le site se construit, et si tout ce passe bien vous devriez obtenir quelques chose comme :

<figure markdown>
  ![Image title](images/index/terminal_run_mkdocs.png){ width="800" align=center loading=lazy }
  <figcaption>Le terminal quand tout ce passe bien !</figcaption>
</figure>

<br/>

Pour pouvoir visualiser votre site dans votre navigateur web, il suffit de suivre le lien.


??? note "Lancer plusieurs container Docker"
    Une image docker peut être utilisée pour lancer plusieurs containers, cependant dans l'images `dev`, il y a aussi la gestion du ports utilisé par le serveur, ce qui interdit l'utilisation de cette images pour rouler plusieurs container docker.

    <br/>

    **La première solution**

    On utilise l'image `prod` et on gère soit même le port.

    Dans un premier temps, on lance l'image `prod` avec le port choisi, par exemple ici le port 8080, et on ouvre le terminal du container `sh`:

    ```shell
    docker run --rm -it -p 8080:8080 -v ${PWD}:/docs goldengolden28/mkdocs-material-plugins-nsi-prod:latest sh
    ```
    Dans un second temps, dans le terminal du container, on lance mkdocs sur le port 8080, avec la ligne de commande :

    ```shell
    mkdocs serve --dev-addr=0.0.0.0:8080
    ```
    *Il faut être vigilent et bien utiliser le port choisit lorsque l'on a lancé l'image docker, ici 8080 !*

    <br/>

    **La seconde solution**

    Si l'on a régulièrement besoin de seulement deux containers mkdocs, on construit une images docker nommée  `dev2` qui lance le service sur un port différent de l'images `dev`, pour l'exemple en choisira comme dans la première solution  le port 8080.

    Pour ce faire on utilise le `dockerfile` de l'image `dev2`:

    *(Ce n'est rien d'autre que le `dockerfile` de l'image `dev` pour laquelle on a changé le port 8000, en 8080)*.

    ```Dockerfile
    FROM goldengolden28/mkdocs-material-plugins-nsi-prod:latest

    # Expose MkDocs development server port
    EXPOSE 8080

    # Start development server by default
    ENTRYPOINT ["mkdocs"]
    CMD ["serve", "--dev-addr=0.0.0.0:8080"]
    ```

    On créer l'images `dev2`:

    On se place dans le même dossier que le `dockerfile`, on lance la commande :

    ```shell
    docker build goldengolden28/mkdocs-material-plugins-nsi-dev2:latest  
    ```

    Pour lancer un nouveau mkdoc, il suffit alors d'utiliser l'image `dev2`.

    ```shell
    docker run --rm -it -p 8080:8080 -v ${PWD}:/docs goldengolden28/mkdocs-material-plugins-nsi-dev2:latest  
    ```
    Cette solution permet de lancer les containers un peu plus rapidement *(notamment garce à la mémoire/autocomplétion du terminal)* !

<br/>

### Faire sans docker
??? note "Si vous n'utilisez pas docker"
    Il sera alors nécessaire d'installer les plugins à la version de python que vous souhaitez utiliser :
    
    Le mieux étant de récupérer le fichier `requirements.txt` présent sur le dépôt
    
    ```txt title="requirements.txt"
    selenium
    mkdocs-material
    mkdocs-macros-plugin
    mkdocs-awesome-pages-plugin
    mkdocs-exclude-search
    mkdocs-kroki-plugin
    git+https://github.com/Epithumia/mkdocs-sqlite-console.git
    ```

    Et de vérifier l'installation des dépendances, en tapant dans votre terminal :

    ```Shell
    $ pip install -r requirements.txt
    ```
    
    Si tout est bon, il suffit alors de ce placer dans le dossier contenant le fichier `mkdocs.yml`, d'ouvrir un terminal et de saisir la  commande :

    ```Shell
    $ mkdocs serve
    ```
    *Ps : Si on veut avoir la main sur le port utilisé, par exemple pour ouvrir plusieurs mkdocs en même temps, on utilise :*
    ```shell
    mkdocs serve --dev-addr=0.0.0.0:8080
    ```



## Éditer le site sur Gitlab
(À venir)

<br/>

## Addaptation du dépot
Dans un premier temps vérifiez que l'intégralité des fonctionnalités du site fonctionnent correctement. Pour cela, il suffit de démarrer le site et de vérifier que tout les exemples présent dans le chapitre Mkdocs ne présente pas de problème.

<br/>

Une fois cette vérification effectuée, il vous est possible de supprimer la totalité du contenue du dossier `docs` à l’exception du dossier `xtra`. Cependant, je vous conseille de ne supprimer que les dossiers dont le nom commence par `0_`, le reste des dossiers et fichier constitue alors un bonne base pour l'organisation de votre site.

<br/>

Il ne vous reste plus qu'à le reramplir le contenue `docs` et de ces sous dossier avec vos cours et vos dossiers et à adapter dans le fichier `mkdocs.yml` la partie :

```yml
    nav :
        - ...|Chapitre_1/*
```
